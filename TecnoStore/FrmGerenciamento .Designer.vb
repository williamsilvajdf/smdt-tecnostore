﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGerenciamento
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblServicosCadastrados = New System.Windows.Forms.Label()
        Me.TsrLoginGerenc = New System.Windows.Forms.ToolStrip()
        Me.LblLegUsuarioLogado = New System.Windows.Forms.ToolStripLabel()
        Me.LblUsuarioLogado = New System.Windows.Forms.ToolStripLabel()
        Me.LblHorarioLogin = New System.Windows.Forms.ToolStripLabel()
        Me.LblHoraDoLogin = New System.Windows.Forms.ToolStripLabel()
        Me.TabGerenciamento = New System.Windows.Forms.TabControl()
        Me.TpDadosOrdemDeServ = New System.Windows.Forms.TabPage()
        Me.LblTotalOS = New System.Windows.Forms.Label()
        Me.TxtTotalOS = New System.Windows.Forms.TextBox()
        Me.BtnSair = New System.Windows.Forms.Button()
        Me.LblTicket = New System.Windows.Forms.Label()
        Me.TxtTicket = New System.Windows.Forms.TextBox()
        Me.BtnNovaOS = New System.Windows.Forms.Button()
        Me.GrpGerenciamento = New System.Windows.Forms.GroupBox()
        Me.LblNomeCliente = New System.Windows.Forms.Label()
        Me.TxtNomeCliente = New System.Windows.Forms.TextBox()
        Me.TxtIdCliente = New System.Windows.Forms.TextBox()
        Me.LblIdCliente = New System.Windows.Forms.Label()
        Me.TpDadosProd = New System.Windows.Forms.TabPage()
        Me.LblTotalProd = New System.Windows.Forms.Label()
        Me.TxtTotalProduto = New System.Windows.Forms.TextBox()
        Me.DgvDetalheProduto = New System.Windows.Forms.DataGridView()
        Me.SelProdutos = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DescriçãoProduto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValorProduto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QtdProduto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VlTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LblValorTotalProd = New System.Windows.Forms.Label()
        Me.LblQtdProduto = New System.Windows.Forms.Label()
        Me.LblValorProduto = New System.Windows.Forms.Label()
        Me.LblDescricaoProduto = New System.Windows.Forms.Label()
        Me.TxtValorTotalProd = New System.Windows.Forms.TextBox()
        Me.TxtQtdProduto = New System.Windows.Forms.TextBox()
        Me.TxtValorProduto = New System.Windows.Forms.TextBox()
        Me.TxtDescricaoProduto = New System.Windows.Forms.TextBox()
        Me.LblIdProduto = New System.Windows.Forms.Label()
        Me.TxtIdProduto = New System.Windows.Forms.TextBox()
        Me.BtnDeletarItemProd = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TpDadosServico = New System.Windows.Forms.TabPage()
        Me.LblTotalServ = New System.Windows.Forms.Label()
        Me.TxtTotalServ = New System.Windows.Forms.TextBox()
        Me.DgvDetalheServico = New System.Windows.Forms.DataGridView()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LblQtdServico = New System.Windows.Forms.Label()
        Me.LblValorServico = New System.Windows.Forms.Label()
        Me.LblDescricaoServico = New System.Windows.Forms.Label()
        Me.TxtQtdServico = New System.Windows.Forms.TextBox()
        Me.TxtValorServico = New System.Windows.Forms.TextBox()
        Me.TxtDescricaoServico = New System.Windows.Forms.TextBox()
        Me.LblIdServico = New System.Windows.Forms.Label()
        Me.TxtIdServico = New System.Windows.Forms.TextBox()
        Me.LblValorTotalServ = New System.Windows.Forms.Label()
        Me.TxtValorTotalServ = New System.Windows.Forms.TextBox()
        Me.BtnDeletarItemServ = New System.Windows.Forms.Button()
        Me.BtnAdicionarServico = New System.Windows.Forms.Button()
        Me.TsrLoginGerenc.SuspendLayout()
        Me.TabGerenciamento.SuspendLayout()
        Me.TpDadosOrdemDeServ.SuspendLayout()
        Me.GrpGerenciamento.SuspendLayout()
        Me.TpDadosProd.SuspendLayout()
        CType(Me.DgvDetalheProduto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TpDadosServico.SuspendLayout()
        CType(Me.DgvDetalheServico, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblServicosCadastrados
        '
        Me.lblServicosCadastrados.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lblServicosCadastrados.AutoSize = True
        Me.lblServicosCadastrados.BackColor = System.Drawing.SystemColors.Control
        Me.lblServicosCadastrados.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicosCadastrados.Location = New System.Drawing.Point(396, 22)
        Me.lblServicosCadastrados.Name = "lblServicosCadastrados"
        Me.lblServicosCadastrados.Size = New System.Drawing.Size(230, 21)
        Me.lblServicosCadastrados.TabIndex = 24
        Me.lblServicosCadastrados.Text = "Ordem de Serviço em Aberto"
        '
        'TsrLoginGerenc
        '
        Me.TsrLoginGerenc.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsrLoginGerenc.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LblLegUsuarioLogado, Me.LblUsuarioLogado, Me.LblHorarioLogin, Me.LblHoraDoLogin})
        Me.TsrLoginGerenc.Location = New System.Drawing.Point(0, 0)
        Me.TsrLoginGerenc.Name = "TsrLoginGerenc"
        Me.TsrLoginGerenc.Size = New System.Drawing.Size(1015, 25)
        Me.TsrLoginGerenc.TabIndex = 25
        '
        'LblLegUsuarioLogado
        '
        Me.LblLegUsuarioLogado.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLegUsuarioLogado.Name = "LblLegUsuarioLogado"
        Me.LblLegUsuarioLogado.Size = New System.Drawing.Size(95, 22)
        Me.LblLegUsuarioLogado.Text = "Usuário Logado:"
        '
        'LblUsuarioLogado
        '
        Me.LblUsuarioLogado.Name = "LblUsuarioLogado"
        Me.LblUsuarioLogado.Size = New System.Drawing.Size(47, 22)
        Me.LblUsuarioLogado.Text = "Usuario"
        '
        'LblHorarioLogin
        '
        Me.LblHorarioLogin.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.LblHorarioLogin.Name = "LblHorarioLogin"
        Me.LblHorarioLogin.Size = New System.Drawing.Size(49, 22)
        Me.LblHorarioLogin.Text = "00:00:00"
        '
        'LblHoraDoLogin
        '
        Me.LblHoraDoLogin.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.LblHoraDoLogin.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblHoraDoLogin.Name = "LblHoraDoLogin"
        Me.LblHoraDoLogin.Size = New System.Drawing.Size(87, 22)
        Me.LblHoraDoLogin.Text = "Hora do Login:"
        '
        'TabGerenciamento
        '
        Me.TabGerenciamento.Controls.Add(Me.TpDadosOrdemDeServ)
        Me.TabGerenciamento.Controls.Add(Me.TpDadosProd)
        Me.TabGerenciamento.Controls.Add(Me.TpDadosServico)
        Me.TabGerenciamento.Location = New System.Drawing.Point(12, 46)
        Me.TabGerenciamento.Name = "TabGerenciamento"
        Me.TabGerenciamento.SelectedIndex = 0
        Me.TabGerenciamento.Size = New System.Drawing.Size(981, 348)
        Me.TabGerenciamento.TabIndex = 46
        '
        'TpDadosOrdemDeServ
        '
        Me.TpDadosOrdemDeServ.Controls.Add(Me.LblTotalOS)
        Me.TpDadosOrdemDeServ.Controls.Add(Me.TxtTotalOS)
        Me.TpDadosOrdemDeServ.Controls.Add(Me.BtnSair)
        Me.TpDadosOrdemDeServ.Controls.Add(Me.LblTicket)
        Me.TpDadosOrdemDeServ.Controls.Add(Me.TxtTicket)
        Me.TpDadosOrdemDeServ.Controls.Add(Me.BtnNovaOS)
        Me.TpDadosOrdemDeServ.Controls.Add(Me.GrpGerenciamento)
        Me.TpDadosOrdemDeServ.Location = New System.Drawing.Point(4, 26)
        Me.TpDadosOrdemDeServ.Name = "TpDadosOrdemDeServ"
        Me.TpDadosOrdemDeServ.Padding = New System.Windows.Forms.Padding(3)
        Me.TpDadosOrdemDeServ.Size = New System.Drawing.Size(973, 318)
        Me.TpDadosOrdemDeServ.TabIndex = 0
        Me.TpDadosOrdemDeServ.Text = "| Dados da ordem de Serviço |"
        Me.TpDadosOrdemDeServ.UseVisualStyleBackColor = True
        '
        'LblTotalOS
        '
        Me.LblTotalOS.AutoSize = True
        Me.LblTotalOS.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTotalOS.Location = New System.Drawing.Point(639, 129)
        Me.LblTotalOS.Name = "LblTotalOS"
        Me.LblTotalOS.Size = New System.Drawing.Size(88, 20)
        Me.LblTotalOS.TabIndex = 36
        Me.LblTotalOS.Text = "Total da OS"
        '
        'TxtTotalOS
        '
        Me.TxtTotalOS.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTotalOS.Location = New System.Drawing.Point(615, 155)
        Me.TxtTotalOS.Name = "TxtTotalOS"
        Me.TxtTotalOS.Size = New System.Drawing.Size(137, 33)
        Me.TxtTotalOS.TabIndex = 37
        Me.TxtTotalOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BtnSair
        '
        Me.BtnSair.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSair.Image = Global.TecnoStore.My.Resources.Resources._Exit
        Me.BtnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnSair.Location = New System.Drawing.Point(814, 264)
        Me.BtnSair.Name = "BtnSair"
        Me.BtnSair.Size = New System.Drawing.Size(139, 36)
        Me.BtnSair.TabIndex = 35
        Me.BtnSair.Text = "Sair"
        Me.BtnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnSair.UseVisualStyleBackColor = True
        '
        'LblTicket
        '
        Me.LblTicket.AutoSize = True
        Me.LblTicket.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTicket.Location = New System.Drawing.Point(25, 28)
        Me.LblTicket.Name = "LblTicket"
        Me.LblTicket.Size = New System.Drawing.Size(85, 17)
        Me.LblTicket.TabIndex = 34
        Me.LblTicket.Text = "Ticket da OS"
        '
        'TxtTicket
        '
        Me.TxtTicket.BackColor = System.Drawing.Color.LightGray
        Me.TxtTicket.Location = New System.Drawing.Point(19, 48)
        Me.TxtTicket.Name = "TxtTicket"
        Me.TxtTicket.Size = New System.Drawing.Size(91, 25)
        Me.TxtTicket.TabIndex = 32
        '
        'BtnNovaOS
        '
        Me.BtnNovaOS.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnNovaOS.Image = Global.TecnoStore.My.Resources.Resources._95
        Me.BtnNovaOS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnNovaOS.Location = New System.Drawing.Point(814, 36)
        Me.BtnNovaOS.Name = "BtnNovaOS"
        Me.BtnNovaOS.Size = New System.Drawing.Size(139, 37)
        Me.BtnNovaOS.TabIndex = 31
        Me.BtnNovaOS.Text = "Nova"
        Me.BtnNovaOS.UseVisualStyleBackColor = True
        '
        'GrpGerenciamento
        '
        Me.GrpGerenciamento.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GrpGerenciamento.Controls.Add(Me.LblNomeCliente)
        Me.GrpGerenciamento.Controls.Add(Me.TxtNomeCliente)
        Me.GrpGerenciamento.Controls.Add(Me.TxtIdCliente)
        Me.GrpGerenciamento.Controls.Add(Me.LblIdCliente)
        Me.GrpGerenciamento.Location = New System.Drawing.Point(19, 91)
        Me.GrpGerenciamento.Name = "GrpGerenciamento"
        Me.GrpGerenciamento.Size = New System.Drawing.Size(590, 97)
        Me.GrpGerenciamento.TabIndex = 33
        Me.GrpGerenciamento.TabStop = False
        Me.GrpGerenciamento.Text = "Dados do Cliente"
        '
        'LblNomeCliente
        '
        Me.LblNomeCliente.AutoSize = True
        Me.LblNomeCliente.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomeCliente.Location = New System.Drawing.Point(144, 31)
        Me.LblNomeCliente.Name = "LblNomeCliente"
        Me.LblNomeCliente.Size = New System.Drawing.Size(45, 17)
        Me.LblNomeCliente.TabIndex = 6
        Me.LblNomeCliente.Text = "Nome"
        '
        'TxtNomeCliente
        '
        Me.TxtNomeCliente.Location = New System.Drawing.Point(138, 51)
        Me.TxtNomeCliente.Name = "TxtNomeCliente"
        Me.TxtNomeCliente.Size = New System.Drawing.Size(443, 25)
        Me.TxtNomeCliente.TabIndex = 5
        '
        'TxtIdCliente
        '
        Me.TxtIdCliente.BackColor = System.Drawing.SystemColors.Window
        Me.TxtIdCliente.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtIdCliente.Location = New System.Drawing.Point(15, 51)
        Me.TxtIdCliente.MaxLength = 9
        Me.TxtIdCliente.Name = "TxtIdCliente"
        Me.TxtIdCliente.Size = New System.Drawing.Size(91, 25)
        Me.TxtIdCliente.TabIndex = 4
        Me.TxtIdCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblIdCliente
        '
        Me.LblIdCliente.AutoSize = True
        Me.LblIdCliente.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblIdCliente.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LblIdCliente.Location = New System.Drawing.Point(12, 31)
        Me.LblIdCliente.Name = "LblIdCliente"
        Me.LblIdCliente.Size = New System.Drawing.Size(89, 17)
        Me.LblIdCliente.TabIndex = 2
        Me.LblIdCliente.Text = "ID do Cliente"
        '
        'TpDadosProd
        '
        Me.TpDadosProd.Controls.Add(Me.LblTotalProd)
        Me.TpDadosProd.Controls.Add(Me.TxtTotalProduto)
        Me.TpDadosProd.Controls.Add(Me.DgvDetalheProduto)
        Me.TpDadosProd.Controls.Add(Me.LblValorTotalProd)
        Me.TpDadosProd.Controls.Add(Me.LblQtdProduto)
        Me.TpDadosProd.Controls.Add(Me.LblValorProduto)
        Me.TpDadosProd.Controls.Add(Me.LblDescricaoProduto)
        Me.TpDadosProd.Controls.Add(Me.TxtValorTotalProd)
        Me.TpDadosProd.Controls.Add(Me.TxtQtdProduto)
        Me.TpDadosProd.Controls.Add(Me.TxtValorProduto)
        Me.TpDadosProd.Controls.Add(Me.TxtDescricaoProduto)
        Me.TpDadosProd.Controls.Add(Me.LblIdProduto)
        Me.TpDadosProd.Controls.Add(Me.TxtIdProduto)
        Me.TpDadosProd.Controls.Add(Me.BtnDeletarItemProd)
        Me.TpDadosProd.Controls.Add(Me.Button2)
        Me.TpDadosProd.Location = New System.Drawing.Point(4, 26)
        Me.TpDadosProd.Name = "TpDadosProd"
        Me.TpDadosProd.Padding = New System.Windows.Forms.Padding(3)
        Me.TpDadosProd.Size = New System.Drawing.Size(973, 318)
        Me.TpDadosProd.TabIndex = 1
        Me.TpDadosProd.Text = "| Dados do Produto |"
        Me.TpDadosProd.UseVisualStyleBackColor = True
        '
        'LblTotalProd
        '
        Me.LblTotalProd.AutoSize = True
        Me.LblTotalProd.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTotalProd.Location = New System.Drawing.Point(641, 44)
        Me.LblTotalProd.Name = "LblTotalProd"
        Me.LblTotalProd.Size = New System.Drawing.Size(57, 17)
        Me.LblTotalProd.TabIndex = 48
        Me.LblTotalProd.Text = "Total R$"
        '
        'TxtTotalProduto
        '
        Me.TxtTotalProduto.Location = New System.Drawing.Point(619, 64)
        Me.TxtTotalProduto.Name = "TxtTotalProduto"
        Me.TxtTotalProduto.Size = New System.Drawing.Size(96, 25)
        Me.TxtTotalProduto.TabIndex = 47
        '
        'DgvDetalheProduto
        '
        Me.DgvDetalheProduto.AllowUserToAddRows = False
        Me.DgvDetalheProduto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvDetalheProduto.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SelProdutos, Me.DescriçãoProduto, Me.ValorProduto, Me.QtdProduto, Me.VlTotal})
        Me.DgvDetalheProduto.Location = New System.Drawing.Point(43, 106)
        Me.DgvDetalheProduto.Name = "DgvDetalheProduto"
        Me.DgvDetalheProduto.Size = New System.Drawing.Size(672, 137)
        Me.DgvDetalheProduto.TabIndex = 46
        '
        'SelProdutos
        '
        Me.SelProdutos.HeaderText = "Seleção"
        Me.SelProdutos.Name = "SelProdutos"
        Me.SelProdutos.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SelProdutos.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DescriçãoProduto
        '
        Me.DescriçãoProduto.HeaderText = "Descrição"
        Me.DescriçãoProduto.Name = "DescriçãoProduto"
        Me.DescriçãoProduto.Width = 150
        '
        'ValorProduto
        '
        Me.ValorProduto.HeaderText = "Valor R$"
        Me.ValorProduto.Name = "ValorProduto"
        '
        'QtdProduto
        '
        Me.QtdProduto.HeaderText = "Qtd Produto"
        Me.QtdProduto.Name = "QtdProduto"
        Me.QtdProduto.Width = 115
        '
        'VlTotal
        '
        Me.VlTotal.HeaderText = "Valor Total"
        Me.VlTotal.Name = "VlTotal"
        '
        'LblValorTotalProd
        '
        Me.LblValorTotalProd.AutoSize = True
        Me.LblValorTotalProd.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValorTotalProd.Location = New System.Drawing.Point(741, 194)
        Me.LblValorTotalProd.Name = "LblValorTotalProd"
        Me.LblValorTotalProd.Size = New System.Drawing.Size(159, 17)
        Me.LblValorTotalProd.TabIndex = 41
        Me.LblValorTotalProd.Text = "Valor Total dos Produtos"
        '
        'LblQtdProduto
        '
        Me.LblQtdProduto.AutoSize = True
        Me.LblQtdProduto.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblQtdProduto.Location = New System.Drawing.Point(514, 44)
        Me.LblQtdProduto.Name = "LblQtdProduto"
        Me.LblQtdProduto.Size = New System.Drawing.Size(79, 17)
        Me.LblQtdProduto.TabIndex = 42
        Me.LblQtdProduto.Text = "Quantidade"
        '
        'LblValorProduto
        '
        Me.LblValorProduto.AutoSize = True
        Me.LblValorProduto.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValorProduto.Location = New System.Drawing.Point(418, 44)
        Me.LblValorProduto.Name = "LblValorProduto"
        Me.LblValorProduto.Size = New System.Drawing.Size(58, 17)
        Me.LblValorProduto.TabIndex = 43
        Me.LblValorProduto.Text = "Valor R$"
        '
        'LblDescricaoProduto
        '
        Me.LblDescricaoProduto.AutoSize = True
        Me.LblDescricaoProduto.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDescricaoProduto.Location = New System.Drawing.Point(176, 44)
        Me.LblDescricaoProduto.Name = "LblDescricaoProduto"
        Me.LblDescricaoProduto.Size = New System.Drawing.Size(65, 17)
        Me.LblDescricaoProduto.TabIndex = 44
        Me.LblDescricaoProduto.Text = "Descrição"
        '
        'TxtValorTotalProd
        '
        Me.TxtValorTotalProd.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.TxtValorTotalProd.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtValorTotalProd.Location = New System.Drawing.Point(737, 214)
        Me.TxtValorTotalProd.Name = "TxtValorTotalProd"
        Me.TxtValorTotalProd.Size = New System.Drawing.Size(163, 29)
        Me.TxtValorTotalProd.TabIndex = 37
        '
        'TxtQtdProduto
        '
        Me.TxtQtdProduto.Location = New System.Drawing.Point(498, 64)
        Me.TxtQtdProduto.Name = "TxtQtdProduto"
        Me.TxtQtdProduto.Size = New System.Drawing.Size(115, 25)
        Me.TxtQtdProduto.TabIndex = 38
        '
        'TxtValorProduto
        '
        Me.TxtValorProduto.Location = New System.Drawing.Point(396, 64)
        Me.TxtValorProduto.Name = "TxtValorProduto"
        Me.TxtValorProduto.ReadOnly = True
        Me.TxtValorProduto.Size = New System.Drawing.Size(96, 25)
        Me.TxtValorProduto.TabIndex = 39
        '
        'TxtDescricaoProduto
        '
        Me.TxtDescricaoProduto.Location = New System.Drawing.Point(170, 64)
        Me.TxtDescricaoProduto.Name = "TxtDescricaoProduto"
        Me.TxtDescricaoProduto.ReadOnly = True
        Me.TxtDescricaoProduto.Size = New System.Drawing.Size(220, 25)
        Me.TxtDescricaoProduto.TabIndex = 40
        '
        'LblIdProduto
        '
        Me.LblIdProduto.AutoSize = True
        Me.LblIdProduto.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblIdProduto.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LblIdProduto.Location = New System.Drawing.Point(44, 44)
        Me.LblIdProduto.Name = "LblIdProduto"
        Me.LblIdProduto.Size = New System.Drawing.Size(96, 17)
        Me.LblIdProduto.TabIndex = 35
        Me.LblIdProduto.Text = "ID do Produto"
        '
        'TxtIdProduto
        '
        Me.TxtIdProduto.BackColor = System.Drawing.SystemColors.Window
        Me.TxtIdProduto.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtIdProduto.Location = New System.Drawing.Point(47, 64)
        Me.TxtIdProduto.MaxLength = 9
        Me.TxtIdProduto.Name = "TxtIdProduto"
        Me.TxtIdProduto.Size = New System.Drawing.Size(91, 25)
        Me.TxtIdProduto.TabIndex = 36
        Me.TxtIdProduto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BtnDeletarItemProd
        '
        Me.BtnDeletarItemProd.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDeletarItemProd.Image = Global.TecnoStore.My.Resources.Resources.Delete
        Me.BtnDeletarItemProd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnDeletarItemProd.Location = New System.Drawing.Point(737, 146)
        Me.BtnDeletarItemProd.Name = "BtnDeletarItemProd"
        Me.BtnDeletarItemProd.Size = New System.Drawing.Size(167, 36)
        Me.BtnDeletarItemProd.TabIndex = 45
        Me.BtnDeletarItemProd.Text = "Deletar item"
        Me.BtnDeletarItemProd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDeletarItemProd.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Image = Global.TecnoStore.My.Resources.Resources._95
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(737, 106)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(167, 37)
        Me.Button2.TabIndex = 34
        Me.Button2.Text = "Adicionar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TpDadosServico
        '
        Me.TpDadosServico.Controls.Add(Me.LblTotalServ)
        Me.TpDadosServico.Controls.Add(Me.TxtTotalServ)
        Me.TpDadosServico.Controls.Add(Me.DgvDetalheServico)
        Me.TpDadosServico.Controls.Add(Me.LblQtdServico)
        Me.TpDadosServico.Controls.Add(Me.LblValorServico)
        Me.TpDadosServico.Controls.Add(Me.LblDescricaoServico)
        Me.TpDadosServico.Controls.Add(Me.TxtQtdServico)
        Me.TpDadosServico.Controls.Add(Me.TxtValorServico)
        Me.TpDadosServico.Controls.Add(Me.TxtDescricaoServico)
        Me.TpDadosServico.Controls.Add(Me.LblIdServico)
        Me.TpDadosServico.Controls.Add(Me.TxtIdServico)
        Me.TpDadosServico.Controls.Add(Me.LblValorTotalServ)
        Me.TpDadosServico.Controls.Add(Me.TxtValorTotalServ)
        Me.TpDadosServico.Controls.Add(Me.BtnDeletarItemServ)
        Me.TpDadosServico.Controls.Add(Me.BtnAdicionarServico)
        Me.TpDadosServico.Location = New System.Drawing.Point(4, 26)
        Me.TpDadosServico.Name = "TpDadosServico"
        Me.TpDadosServico.Padding = New System.Windows.Forms.Padding(3)
        Me.TpDadosServico.Size = New System.Drawing.Size(973, 318)
        Me.TpDadosServico.TabIndex = 2
        Me.TpDadosServico.Text = "| Dados do Serviço |"
        Me.TpDadosServico.UseVisualStyleBackColor = True
        '
        'LblTotalServ
        '
        Me.LblTotalServ.AutoSize = True
        Me.LblTotalServ.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTotalServ.Location = New System.Drawing.Point(618, 15)
        Me.LblTotalServ.Name = "LblTotalServ"
        Me.LblTotalServ.Size = New System.Drawing.Size(57, 17)
        Me.LblTotalServ.TabIndex = 59
        Me.LblTotalServ.Text = "Total R$"
        '
        'TxtTotalServ
        '
        Me.TxtTotalServ.Location = New System.Drawing.Point(596, 35)
        Me.TxtTotalServ.Name = "TxtTotalServ"
        Me.TxtTotalServ.Size = New System.Drawing.Size(96, 25)
        Me.TxtTotalServ.TabIndex = 58
        '
        'DgvDetalheServico
        '
        Me.DgvDetalheServico.AllowUserToAddRows = False
        Me.DgvDetalheServico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvDetalheServico.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.DgvDetalheServico.Location = New System.Drawing.Point(20, 77)
        Me.DgvDetalheServico.Name = "DgvDetalheServico"
        Me.DgvDetalheServico.Size = New System.Drawing.Size(672, 137)
        Me.DgvDetalheServico.TabIndex = 57
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Seleção"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Descrição"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Valor R$"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Qtd Produto"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 115
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Valor Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'LblQtdServico
        '
        Me.LblQtdServico.AutoSize = True
        Me.LblQtdServico.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblQtdServico.Location = New System.Drawing.Point(491, 15)
        Me.LblQtdServico.Name = "LblQtdServico"
        Me.LblQtdServico.Size = New System.Drawing.Size(79, 17)
        Me.LblQtdServico.TabIndex = 54
        Me.LblQtdServico.Text = "Quantidade"
        '
        'LblValorServico
        '
        Me.LblValorServico.AutoSize = True
        Me.LblValorServico.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValorServico.Location = New System.Drawing.Point(392, 15)
        Me.LblValorServico.Name = "LblValorServico"
        Me.LblValorServico.Size = New System.Drawing.Size(58, 17)
        Me.LblValorServico.TabIndex = 55
        Me.LblValorServico.Text = "Valor R$"
        '
        'LblDescricaoServico
        '
        Me.LblDescricaoServico.AutoSize = True
        Me.LblDescricaoServico.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDescricaoServico.Location = New System.Drawing.Point(153, 15)
        Me.LblDescricaoServico.Name = "LblDescricaoServico"
        Me.LblDescricaoServico.Size = New System.Drawing.Size(65, 17)
        Me.LblDescricaoServico.TabIndex = 56
        Me.LblDescricaoServico.Text = "Descrição"
        '
        'TxtQtdServico
        '
        Me.TxtQtdServico.Location = New System.Drawing.Point(475, 35)
        Me.TxtQtdServico.Name = "TxtQtdServico"
        Me.TxtQtdServico.Size = New System.Drawing.Size(115, 25)
        Me.TxtQtdServico.TabIndex = 51
        '
        'TxtValorServico
        '
        Me.TxtValorServico.Location = New System.Drawing.Point(373, 35)
        Me.TxtValorServico.Name = "TxtValorServico"
        Me.TxtValorServico.ReadOnly = True
        Me.TxtValorServico.Size = New System.Drawing.Size(96, 25)
        Me.TxtValorServico.TabIndex = 52
        '
        'TxtDescricaoServico
        '
        Me.TxtDescricaoServico.Location = New System.Drawing.Point(147, 35)
        Me.TxtDescricaoServico.Name = "TxtDescricaoServico"
        Me.TxtDescricaoServico.ReadOnly = True
        Me.TxtDescricaoServico.Size = New System.Drawing.Size(220, 25)
        Me.TxtDescricaoServico.TabIndex = 53
        '
        'LblIdServico
        '
        Me.LblIdServico.AutoSize = True
        Me.LblIdServico.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblIdServico.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LblIdServico.Location = New System.Drawing.Point(21, 15)
        Me.LblIdServico.Name = "LblIdServico"
        Me.LblIdServico.Size = New System.Drawing.Size(96, 17)
        Me.LblIdServico.TabIndex = 49
        Me.LblIdServico.Text = "ID do Produto"
        '
        'TxtIdServico
        '
        Me.TxtIdServico.BackColor = System.Drawing.SystemColors.Window
        Me.TxtIdServico.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtIdServico.Location = New System.Drawing.Point(24, 35)
        Me.TxtIdServico.MaxLength = 9
        Me.TxtIdServico.Name = "TxtIdServico"
        Me.TxtIdServico.Size = New System.Drawing.Size(91, 25)
        Me.TxtIdServico.TabIndex = 50
        Me.TxtIdServico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblValorTotalServ
        '
        Me.LblValorTotalServ.AutoSize = True
        Me.LblValorTotalServ.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValorTotalServ.Location = New System.Drawing.Point(729, 165)
        Me.LblValorTotalServ.Name = "LblValorTotalServ"
        Me.LblValorTotalServ.Size = New System.Drawing.Size(152, 17)
        Me.LblValorTotalServ.TabIndex = 47
        Me.LblValorTotalServ.Text = "Valor Total dos Serviços"
        '
        'TxtValorTotalServ
        '
        Me.TxtValorTotalServ.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.TxtValorTotalServ.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtValorTotalServ.Location = New System.Drawing.Point(718, 185)
        Me.TxtValorTotalServ.Name = "TxtValorTotalServ"
        Me.TxtValorTotalServ.Size = New System.Drawing.Size(163, 29)
        Me.TxtValorTotalServ.TabIndex = 46
        '
        'BtnDeletarItemServ
        '
        Me.BtnDeletarItemServ.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDeletarItemServ.Image = Global.TecnoStore.My.Resources.Resources.Delete
        Me.BtnDeletarItemServ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnDeletarItemServ.Location = New System.Drawing.Point(718, 120)
        Me.BtnDeletarItemServ.Name = "BtnDeletarItemServ"
        Me.BtnDeletarItemServ.Size = New System.Drawing.Size(167, 36)
        Me.BtnDeletarItemServ.TabIndex = 48
        Me.BtnDeletarItemServ.Text = "Deletar item"
        Me.BtnDeletarItemServ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDeletarItemServ.UseVisualStyleBackColor = True
        '
        'BtnAdicionarServico
        '
        Me.BtnAdicionarServico.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAdicionarServico.Image = Global.TecnoStore.My.Resources.Resources._95
        Me.BtnAdicionarServico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnAdicionarServico.Location = New System.Drawing.Point(718, 77)
        Me.BtnAdicionarServico.Name = "BtnAdicionarServico"
        Me.BtnAdicionarServico.Size = New System.Drawing.Size(167, 37)
        Me.BtnAdicionarServico.TabIndex = 45
        Me.BtnAdicionarServico.Text = "Adicionar"
        Me.BtnAdicionarServico.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnAdicionarServico.UseVisualStyleBackColor = True
        '
        'FrmGerenciamento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.ClientSize = New System.Drawing.Size(1015, 407)
        Me.Controls.Add(Me.TabGerenciamento)
        Me.Controls.Add(Me.TsrLoginGerenc)
        Me.Controls.Add(Me.lblServicosCadastrados)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "FrmGerenciamento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TecnoStore — Gerenciamento"
        Me.TsrLoginGerenc.ResumeLayout(False)
        Me.TsrLoginGerenc.PerformLayout()
        Me.TabGerenciamento.ResumeLayout(False)
        Me.TpDadosOrdemDeServ.ResumeLayout(False)
        Me.TpDadosOrdemDeServ.PerformLayout()
        Me.GrpGerenciamento.ResumeLayout(False)
        Me.GrpGerenciamento.PerformLayout()
        Me.TpDadosProd.ResumeLayout(False)
        Me.TpDadosProd.PerformLayout()
        CType(Me.DgvDetalheProduto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TpDadosServico.ResumeLayout(False)
        Me.TpDadosServico.PerformLayout()
        CType(Me.DgvDetalheServico, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblServicosCadastrados As Label
    Friend WithEvents TsrLoginGerenc As ToolStrip
    Friend WithEvents LblLegUsuarioLogado As ToolStripLabel
    Friend WithEvents LblUsuarioLogado As ToolStripLabel
    Friend WithEvents LblHorarioLogin As ToolStripLabel
    Friend WithEvents LblHoraDoLogin As ToolStripLabel
    Friend WithEvents TabGerenciamento As System.Windows.Forms.TabControl
    Friend WithEvents TpDadosOrdemDeServ As System.Windows.Forms.TabPage
    Friend WithEvents LblTotalOS As System.Windows.Forms.Label
    Friend WithEvents TxtTotalOS As System.Windows.Forms.TextBox
    Friend WithEvents BtnSair As System.Windows.Forms.Button
    Friend WithEvents LblTicket As System.Windows.Forms.Label
    Friend WithEvents TxtTicket As System.Windows.Forms.TextBox
    Friend WithEvents BtnNovaOS As System.Windows.Forms.Button
    Friend WithEvents GrpGerenciamento As System.Windows.Forms.GroupBox
    Friend WithEvents LblNomeCliente As System.Windows.Forms.Label
    Friend WithEvents TxtNomeCliente As System.Windows.Forms.TextBox
    Friend WithEvents TxtIdCliente As System.Windows.Forms.TextBox
    Friend WithEvents LblIdCliente As System.Windows.Forms.Label
    Friend WithEvents TpDadosProd As System.Windows.Forms.TabPage
    Friend WithEvents LblTotalProd As System.Windows.Forms.Label
    Friend WithEvents TxtTotalProduto As System.Windows.Forms.TextBox
    Friend WithEvents DgvDetalheProduto As System.Windows.Forms.DataGridView
    Friend WithEvents SelProdutos As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DescriçãoProduto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValorProduto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QtdProduto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VlTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblValorTotalProd As System.Windows.Forms.Label
    Friend WithEvents LblQtdProduto As System.Windows.Forms.Label
    Friend WithEvents LblValorProduto As System.Windows.Forms.Label
    Friend WithEvents LblDescricaoProduto As System.Windows.Forms.Label
    Friend WithEvents TxtValorTotalProd As System.Windows.Forms.TextBox
    Friend WithEvents TxtQtdProduto As System.Windows.Forms.TextBox
    Friend WithEvents TxtValorProduto As System.Windows.Forms.TextBox
    Friend WithEvents TxtDescricaoProduto As System.Windows.Forms.TextBox
    Friend WithEvents LblIdProduto As System.Windows.Forms.Label
    Friend WithEvents TxtIdProduto As System.Windows.Forms.TextBox
    Friend WithEvents BtnDeletarItemProd As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TpDadosServico As System.Windows.Forms.TabPage
    Friend WithEvents LblTotalServ As System.Windows.Forms.Label
    Friend WithEvents TxtTotalServ As System.Windows.Forms.TextBox
    Friend WithEvents DgvDetalheServico As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblQtdServico As System.Windows.Forms.Label
    Friend WithEvents LblValorServico As System.Windows.Forms.Label
    Friend WithEvents LblDescricaoServico As System.Windows.Forms.Label
    Friend WithEvents TxtQtdServico As System.Windows.Forms.TextBox
    Friend WithEvents TxtValorServico As System.Windows.Forms.TextBox
    Friend WithEvents TxtDescricaoServico As System.Windows.Forms.TextBox
    Friend WithEvents LblIdServico As System.Windows.Forms.Label
    Friend WithEvents TxtIdServico As System.Windows.Forms.TextBox
    Friend WithEvents LblValorTotalServ As System.Windows.Forms.Label
    Friend WithEvents TxtValorTotalServ As System.Windows.Forms.TextBox
    Friend WithEvents BtnDeletarItemServ As System.Windows.Forms.Button
    Friend WithEvents BtnAdicionarServico As System.Windows.Forms.Button
End Class
