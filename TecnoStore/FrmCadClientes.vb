﻿Option Strict Off
Public Class FrmCadClientes
    Public TbClientes As DAO.Recordset()
    Dim SQL As String
    Dim X As Integer
    Dim Existe As Boolean

    Private Sub FrmCadClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MontarGradeClientes() 'Carrega a base de clientes cadastros no DgvCadClientes logo quando o formulário é aberto
    End Sub

    Private Sub BtnBuscarCEP_Click(sender As Object, e As EventArgs) Handles BtnBuscarCEP.Click
        Try
            Dim WS = New WSCorreios.AtendeClienteClient()
            Dim Resposta = WS.consultaCEP(MskCEP.Text)
            TxtRua.Text = Resposta.end
            TxtComplemento.Text = Resposta.complemento2
            TxtBairro.Text = Resposta.bairro
            TxtCidade.Text = Resposta.cidade
            CboUF.Text = Resposta.uf
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub LimparCampos() 'Limpa as caixas de texto do FrmCadClientes
        TxtNomeCliente.Clear()
        MskCPF.Clear()
        MskTelefone.Clear()
        MskCelular.Clear()
        TxtEmail.Clear()
        TxtRua.Clear()
        TxtNumero.Clear()
        TxtComplemento.Clear()
        TxtBairro.Clear()
        MskCEP.Clear()
        TxtCidade.Clear()
        CboUF.Text = ""
        TxtNomeCliente.Focus()
    End Sub

    Private Sub BtnNovo_Click(sender As Object, e As EventArgs) Handles BtnNovo.Click
        LimparCampos()
    End Sub

    Private Sub SalvarCadastroCliente() 'Salva no banco de dados, as informações digitadas no FrmCadClientes
        Pesquisa = Base.OpenRecordset("TbClientes")
        Pesquisa.AddNew()
        Try
            'Pesquisa(0).Value = Selecao --> a posição 0 está reservada para a Coluna Seleção
            Pesquisa(1).Value = TxtNomeCliente.Text.ToString.ToUpper
            Pesquisa(2).Value = MskCPF.Text
            Pesquisa(3).Value = MskTelefone.Text
            Pesquisa(4).Value = MskCelular.Text
            Pesquisa(5).Value = TxtEmail.Text.ToString.ToUpper
            Pesquisa(6).Value = TxtRua.Text.ToString.ToUpper
            Pesquisa(7).Value = TxtNumero.Text
            Pesquisa(8).Value = TxtComplemento.Text.ToString.ToUpper
            Pesquisa(9).Value = MskCEP.Text
            Pesquisa(10).Value = TxtBairro.Text.ToString.ToUpper
            Pesquisa(11).Value = TxtCidade.Text.ToString.ToUpper
            Pesquisa(12).Value = CboUF.Text.ToString
            Pesquisa(13).Value = True
            Pesquisa.Update()
            Pesquisa.Close()
            TxtNomeCliente.Focus()
        Catch ex As Exception
            MsgBox("Este cliente já se encontra cadastrado!", vbInformation, "Cliente já cadastrado...")
        End Try
    End Sub

    Private Sub MontarGradeClientes()
        With DgvCadClientes()
            .Rows.Clear()
            X = 0
            SQL = "SELECT * FROM TbClientes WHERE Ativo = True ORDER BY Id"
            Pesquisa = Base.OpenRecordset(SQL)

            If Pesquisa.RecordCount <> 0 Then
                Pesquisa.MoveFirst()

                While Pesquisa.EOF = False
                    .Rows.Add(False)
                    .Item(1, X).Value = Pesquisa.Fields("Id").Value
                    .Item(2, X).Value = Pesquisa.Fields("Nome").Value
                    .Item(3, X).Value = Pesquisa.Fields("CPF").Value
                    .Item(4, X).Value = Pesquisa.Fields("Telefone").Value
                    .Item(5, X).Value = Pesquisa.Fields("Celular").Value
                    .Item(6, X).Value = Pesquisa.Fields("Email").Value
                    .Item(7, X).Value = Pesquisa.Fields("Rua").Value
                    .Item(8, X).Value = Pesquisa.Fields("Numero").Value
                    .Item(9, X).Value = Pesquisa.Fields("Complemento").Value
                    .Item(10, X).Value = Pesquisa.Fields("CEP").Value
                    .Item(11, X).Value = Pesquisa.Fields("Bairro").Value
                    .Item(12, X).Value = Pesquisa.Fields("Cidade").Value
                    .Item(13, X).Value = Pesquisa.Fields("UF").Value
                    X += 1 'o mesmo que X = X + 1
                    Pesquisa.MoveNext()
                End While
            End If
        End With
    End Sub

    Private Sub BtnSalvar_Click(sender As Object, e As EventArgs) Handles BtnSalvar.Click
        SalvarCadastroCliente()
        MontarGradeClientes()
    End Sub

    Private Sub BtnSair_Click(sender As Object, e As EventArgs) Handles BtnSair.Click
        Close() 'Fecha o FrmCadClientes
    End Sub
End Class