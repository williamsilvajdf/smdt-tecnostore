﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCadServicos
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblServicosCadastrados = New System.Windows.Forms.Label()
        Me.PnlCadastroDeDados = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BtnNovo = New System.Windows.Forms.Button()
        Me.BtnSalvar = New System.Windows.Forms.Button()
        Me.LblValorServico = New System.Windows.Forms.Label()
        Me.TxtValorServico = New System.Windows.Forms.TextBox()
        Me.LblDescricaoServico = New System.Windows.Forms.Label()
        Me.TxtDescricaoServico = New System.Windows.Forms.TextBox()
        Me.PnlGerenciaDeDados = New System.Windows.Forms.Panel()
        Me.PbBuscarServico = New System.Windows.Forms.PictureBox()
        Me.LblBuscarCadastro = New System.Windows.Forms.Label()
        Me.TxtBuscarCadastro = New System.Windows.Forms.TextBox()
        Me.DgvCadServicos = New System.Windows.Forms.DataGridView()
        Me.Selecao = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IdCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescricaoServico = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Valor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GrpGerenciamento = New System.Windows.Forms.GroupBox()
        Me.BtnSair = New System.Windows.Forms.Button()
        Me.BtnDeletar = New System.Windows.Forms.Button()
        Me.BtnAlterar = New System.Windows.Forms.Button()
        Me.BtnConsultar = New System.Windows.Forms.Button()
        Me.TxtIdServico = New System.Windows.Forms.TextBox()
        Me.LblID = New System.Windows.Forms.Label()
        Me.PnlCadastroDeDados.SuspendLayout()
        Me.PnlGerenciaDeDados.SuspendLayout()
        CType(Me.PbBuscarServico, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvCadServicos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpGerenciamento.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblServicosCadastrados
        '
        Me.lblServicosCadastrados.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lblServicosCadastrados.AutoSize = True
        Me.lblServicosCadastrados.BackColor = System.Drawing.SystemColors.Control
        Me.lblServicosCadastrados.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicosCadastrados.Location = New System.Drawing.Point(516, 7)
        Me.lblServicosCadastrados.Name = "lblServicosCadastrados"
        Me.lblServicosCadastrados.Size = New System.Drawing.Size(259, 21)
        Me.lblServicosCadastrados.TabIndex = 27
        Me.lblServicosCadastrados.Text = "Serviços Cadastrados no Sistema"
        '
        'PnlCadastroDeDados
        '
        Me.PnlCadastroDeDados.BackColor = System.Drawing.Color.Gainsboro
        Me.PnlCadastroDeDados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlCadastroDeDados.Controls.Add(Me.Label1)
        Me.PnlCadastroDeDados.Controls.Add(Me.BtnNovo)
        Me.PnlCadastroDeDados.Controls.Add(Me.BtnSalvar)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblValorServico)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtValorServico)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblDescricaoServico)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtDescricaoServico)
        Me.PnlCadastroDeDados.Location = New System.Drawing.Point(12, 10)
        Me.PnlCadastroDeDados.Name = "PnlCadastroDeDados"
        Me.PnlCadastroDeDados.Size = New System.Drawing.Size(286, 479)
        Me.PnlCadastroDeDados.TabIndex = 25
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 234)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 20)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "R$"
        '
        'BtnNovo
        '
        Me.BtnNovo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnNovo.Image = Global.TecnoStore.My.Resources.Resources._1
        Me.BtnNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnNovo.Location = New System.Drawing.Point(14, 20)
        Me.BtnNovo.Name = "BtnNovo"
        Me.BtnNovo.Size = New System.Drawing.Size(262, 37)
        Me.BtnNovo.TabIndex = 0
        Me.BtnNovo.Text = "Novo"
        Me.BtnNovo.UseVisualStyleBackColor = True
        '
        'BtnSalvar
        '
        Me.BtnSalvar.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSalvar.Image = Global.TecnoStore.My.Resources.Resources._22
        Me.BtnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnSalvar.Location = New System.Drawing.Point(14, 439)
        Me.BtnSalvar.Name = "BtnSalvar"
        Me.BtnSalvar.Size = New System.Drawing.Size(262, 32)
        Me.BtnSalvar.TabIndex = 3
        Me.BtnSalvar.Text = "Salvar"
        Me.BtnSalvar.UseVisualStyleBackColor = True
        '
        'LblValorServico
        '
        Me.LblValorServico.AutoSize = True
        Me.LblValorServico.Location = New System.Drawing.Point(12, 211)
        Me.LblValorServico.Name = "LblValorServico"
        Me.LblValorServico.Size = New System.Drawing.Size(38, 17)
        Me.LblValorServico.TabIndex = 14
        Me.LblValorServico.Text = "Valor"
        '
        'TxtValorServico
        '
        Me.TxtValorServico.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtValorServico.Location = New System.Drawing.Point(10, 231)
        Me.TxtValorServico.Name = "TxtValorServico"
        Me.TxtValorServico.Size = New System.Drawing.Size(262, 27)
        Me.TxtValorServico.TabIndex = 2
        Me.TxtValorServico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblDescricaoServico
        '
        Me.LblDescricaoServico.AutoSize = True
        Me.LblDescricaoServico.Location = New System.Drawing.Point(12, 116)
        Me.LblDescricaoServico.Name = "LblDescricaoServico"
        Me.LblDescricaoServico.Size = New System.Drawing.Size(131, 17)
        Me.LblDescricaoServico.TabIndex = 2
        Me.LblDescricaoServico.Text = "Descrição do Serviço"
        '
        'TxtDescricaoServico
        '
        Me.TxtDescricaoServico.Location = New System.Drawing.Point(10, 136)
        Me.TxtDescricaoServico.MaxLength = 100
        Me.TxtDescricaoServico.Multiline = True
        Me.TxtDescricaoServico.Name = "TxtDescricaoServico"
        Me.TxtDescricaoServico.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TxtDescricaoServico.Size = New System.Drawing.Size(262, 63)
        Me.TxtDescricaoServico.TabIndex = 1
        Me.TxtDescricaoServico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PnlGerenciaDeDados
        '
        Me.PnlGerenciaDeDados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlGerenciaDeDados.Controls.Add(Me.PbBuscarServico)
        Me.PnlGerenciaDeDados.Controls.Add(Me.LblBuscarCadastro)
        Me.PnlGerenciaDeDados.Controls.Add(Me.TxtBuscarCadastro)
        Me.PnlGerenciaDeDados.Controls.Add(Me.DgvCadServicos)
        Me.PnlGerenciaDeDados.Controls.Add(Me.GrpGerenciamento)
        Me.PnlGerenciaDeDados.Controls.Add(Me.TxtIdServico)
        Me.PnlGerenciaDeDados.Controls.Add(Me.LblID)
        Me.PnlGerenciaDeDados.Location = New System.Drawing.Point(295, 23)
        Me.PnlGerenciaDeDados.Name = "PnlGerenciaDeDados"
        Me.PnlGerenciaDeDados.Size = New System.Drawing.Size(678, 466)
        Me.PnlGerenciaDeDados.TabIndex = 26
        '
        'PbBuscarServico
        '
        Me.PbBuscarServico.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.PbBuscarServico.Image = Global.TecnoStore.My.Resources.Resources.Find
        Me.PbBuscarServico.Location = New System.Drawing.Point(636, 17)
        Me.PbBuscarServico.Name = "PbBuscarServico"
        Me.PbBuscarServico.Size = New System.Drawing.Size(19, 27)
        Me.PbBuscarServico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbBuscarServico.TabIndex = 30
        Me.PbBuscarServico.TabStop = False
        '
        'LblBuscarCadastro
        '
        Me.LblBuscarCadastro.AutoSize = True
        Me.LblBuscarCadastro.Location = New System.Drawing.Point(271, 27)
        Me.LblBuscarCadastro.Name = "LblBuscarCadastro"
        Me.LblBuscarCadastro.Size = New System.Drawing.Size(359, 17)
        Me.LblBuscarCadastro.TabIndex = 25
        Me.LblBuscarCadastro.Text = "Buscar por um cadastro específico | *busque pela Descrição"
        '
        'TxtBuscarCadastro
        '
        Me.TxtBuscarCadastro.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.TxtBuscarCadastro.Location = New System.Drawing.Point(274, 47)
        Me.TxtBuscarCadastro.Name = "TxtBuscarCadastro"
        Me.TxtBuscarCadastro.Size = New System.Drawing.Size(381, 25)
        Me.TxtBuscarCadastro.TabIndex = 5
        Me.TxtBuscarCadastro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DgvCadServicos
        '
        Me.DgvCadServicos.AllowUserToAddRows = False
        Me.DgvCadServicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvCadServicos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Selecao, Me.IdCliente, Me.DescricaoServico, Me.Valor})
        Me.DgvCadServicos.Location = New System.Drawing.Point(8, 151)
        Me.DgvCadServicos.Name = "DgvCadServicos"
        Me.DgvCadServicos.Size = New System.Drawing.Size(647, 307)
        Me.DgvCadServicos.TabIndex = 10
        '
        'Selecao
        '
        Me.Selecao.HeaderText = "Seleção"
        Me.Selecao.Name = "Selecao"
        '
        'IdCliente
        '
        Me.IdCliente.HeaderText = "ID"
        Me.IdCliente.Name = "IdCliente"
        Me.IdCliente.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.IdCliente.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IdCliente.Width = 70
        '
        'DescricaoServico
        '
        Me.DescricaoServico.HeaderText = "Descrição do Serviço"
        Me.DescricaoServico.Name = "DescricaoServico"
        Me.DescricaoServico.Width = 200
        '
        'Valor
        '
        Me.Valor.HeaderText = "Valor R$"
        Me.Valor.Name = "Valor"
        '
        'GrpGerenciamento
        '
        Me.GrpGerenciamento.BackColor = System.Drawing.Color.Linen
        Me.GrpGerenciamento.Controls.Add(Me.BtnSair)
        Me.GrpGerenciamento.Controls.Add(Me.BtnDeletar)
        Me.GrpGerenciamento.Controls.Add(Me.BtnAlterar)
        Me.GrpGerenciamento.Controls.Add(Me.BtnConsultar)
        Me.GrpGerenciamento.Location = New System.Drawing.Point(8, 78)
        Me.GrpGerenciamento.Name = "GrpGerenciamento"
        Me.GrpGerenciamento.Size = New System.Drawing.Size(647, 67)
        Me.GrpGerenciamento.TabIndex = 29
        Me.GrpGerenciamento.TabStop = False
        Me.GrpGerenciamento.Text = "Gerenciamento"
        '
        'BtnSair
        '
        Me.BtnSair.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSair.Image = Global.TecnoStore.My.Resources.Resources._Exit
        Me.BtnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnSair.Location = New System.Drawing.Point(501, 25)
        Me.BtnSair.Name = "BtnSair"
        Me.BtnSair.Size = New System.Drawing.Size(139, 36)
        Me.BtnSair.TabIndex = 9
        Me.BtnSair.Text = "Sair"
        Me.BtnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnSair.UseVisualStyleBackColor = True
        '
        'BtnDeletar
        '
        Me.BtnDeletar.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDeletar.Image = Global.TecnoStore.My.Resources.Resources.Delete
        Me.BtnDeletar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnDeletar.Location = New System.Drawing.Point(328, 24)
        Me.BtnDeletar.Name = "BtnDeletar"
        Me.BtnDeletar.Size = New System.Drawing.Size(167, 36)
        Me.BtnDeletar.TabIndex = 8
        Me.BtnDeletar.Text = "Deletar"
        Me.BtnDeletar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDeletar.UseVisualStyleBackColor = True
        '
        'BtnAlterar
        '
        Me.BtnAlterar.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAlterar.Image = Global.TecnoStore.My.Resources.Resources.Sync
        Me.BtnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnAlterar.Location = New System.Drawing.Point(150, 24)
        Me.BtnAlterar.Name = "BtnAlterar"
        Me.BtnAlterar.Size = New System.Drawing.Size(172, 36)
        Me.BtnAlterar.TabIndex = 7
        Me.BtnAlterar.Text = "Alterar"
        Me.BtnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnAlterar.UseVisualStyleBackColor = True
        '
        'BtnConsultar
        '
        Me.BtnConsultar.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnConsultar.Image = Global.TecnoStore.My.Resources.Resources.Database
        Me.BtnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnConsultar.Location = New System.Drawing.Point(6, 24)
        Me.BtnConsultar.Name = "BtnConsultar"
        Me.BtnConsultar.Size = New System.Drawing.Size(139, 36)
        Me.BtnConsultar.TabIndex = 6
        Me.BtnConsultar.Text = "Consultar"
        Me.BtnConsultar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnConsultar.UseVisualStyleBackColor = True
        '
        'TxtIdServico
        '
        Me.TxtIdServico.BackColor = System.Drawing.Color.LightGray
        Me.TxtIdServico.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtIdServico.Location = New System.Drawing.Point(14, 47)
        Me.TxtIdServico.MaxLength = 9
        Me.TxtIdServico.Name = "TxtIdServico"
        Me.TxtIdServico.Size = New System.Drawing.Size(91, 25)
        Me.TxtIdServico.TabIndex = 4
        Me.TxtIdServico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblID
        '
        Me.LblID.AutoSize = True
        Me.LblID.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblID.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LblID.Location = New System.Drawing.Point(14, 27)
        Me.LblID.Name = "LblID"
        Me.LblID.Size = New System.Drawing.Size(91, 17)
        Me.LblID.TabIndex = 2
        Me.LblID.Text = "ID do Serviço"
        '
        'FrmCadServicos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(985, 496)
        Me.Controls.Add(Me.lblServicosCadastrados)
        Me.Controls.Add(Me.PnlCadastroDeDados)
        Me.Controls.Add(Me.PnlGerenciaDeDados)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "FrmCadServicos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TecnoStore — Cadastro de Serviços"
        Me.PnlCadastroDeDados.ResumeLayout(False)
        Me.PnlCadastroDeDados.PerformLayout()
        Me.PnlGerenciaDeDados.ResumeLayout(False)
        Me.PnlGerenciaDeDados.PerformLayout()
        CType(Me.PbBuscarServico, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvCadServicos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpGerenciamento.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblServicosCadastrados As Label
    Friend WithEvents PnlCadastroDeDados As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents BtnNovo As Button
    Friend WithEvents BtnSalvar As Button
    Friend WithEvents LblValorServico As Label
    Friend WithEvents TxtValorServico As TextBox
    Friend WithEvents LblDescricaoServico As Label
    Friend WithEvents TxtDescricaoServico As TextBox
    Friend WithEvents PnlGerenciaDeDados As Panel
    Friend WithEvents PbBuscarServico As PictureBox
    Friend WithEvents LblBuscarCadastro As Label
    Friend WithEvents TxtBuscarCadastro As TextBox
    Friend WithEvents DgvCadServicos As DataGridView
    Friend WithEvents Selecao As DataGridViewCheckBoxColumn
    Friend WithEvents IdCliente As DataGridViewTextBoxColumn
    Friend WithEvents DescricaoServico As DataGridViewTextBoxColumn
    Friend WithEvents Valor As DataGridViewTextBoxColumn
    Friend WithEvents GrpGerenciamento As GroupBox
    Friend WithEvents BtnSair As Button
    Friend WithEvents BtnDeletar As Button
    Friend WithEvents BtnAlterar As Button
    Friend WithEvents BtnConsultar As Button
    Friend WithEvents TxtIdServico As TextBox
    Friend WithEvents LblID As Label
End Class
