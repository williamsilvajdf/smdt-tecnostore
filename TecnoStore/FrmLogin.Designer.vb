﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLogin
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLogin))
        Me.GrpLogin = New System.Windows.Forms.GroupBox()
        Me.PbImgUsuario = New System.Windows.Forms.PictureBox()
        Me.PbImgSenha = New System.Windows.Forms.PictureBox()
        Me.TxtSenha = New System.Windows.Forms.TextBox()
        Me.BtnLogin = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtNomeUsuario = New System.Windows.Forms.TextBox()
        Me.PbLogo = New System.Windows.Forms.PictureBox()
        Me.BtnSair = New System.Windows.Forms.Button()
        Me.GrpLogin.SuspendLayout()
        CType(Me.PbImgUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbImgSenha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GrpLogin
        '
        Me.GrpLogin.BackColor = System.Drawing.Color.Gainsboro
        Me.GrpLogin.Controls.Add(Me.PbImgUsuario)
        Me.GrpLogin.Controls.Add(Me.PbImgSenha)
        Me.GrpLogin.Controls.Add(Me.TxtSenha)
        Me.GrpLogin.Controls.Add(Me.BtnLogin)
        Me.GrpLogin.Controls.Add(Me.Label2)
        Me.GrpLogin.Controls.Add(Me.Label1)
        Me.GrpLogin.Controls.Add(Me.TxtNomeUsuario)
        Me.GrpLogin.Controls.Add(Me.PbLogo)
        Me.GrpLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GrpLogin.Location = New System.Drawing.Point(13, 12)
        Me.GrpLogin.Name = "GrpLogin"
        Me.GrpLogin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.GrpLogin.Size = New System.Drawing.Size(377, 282)
        Me.GrpLogin.TabIndex = 0
        Me.GrpLogin.TabStop = False
        Me.GrpLogin.Text = "                             Fazer Login no Sistema"
        '
        'PbImgUsuario
        '
        Me.PbImgUsuario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PbImgUsuario.Image = Global.TecnoStore.My.Resources.Resources.User
        Me.PbImgUsuario.Location = New System.Drawing.Point(89, 148)
        Me.PbImgUsuario.Name = "PbImgUsuario"
        Me.PbImgUsuario.Size = New System.Drawing.Size(39, 29)
        Me.PbImgUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbImgUsuario.TabIndex = 5
        Me.PbImgUsuario.TabStop = False
        '
        'PbImgSenha
        '
        Me.PbImgSenha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PbImgSenha.Image = Global.TecnoStore.My.Resources.Resources.Password_protection
        Me.PbImgSenha.Location = New System.Drawing.Point(89, 199)
        Me.PbImgSenha.Name = "PbImgSenha"
        Me.PbImgSenha.Size = New System.Drawing.Size(39, 30)
        Me.PbImgSenha.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbImgSenha.TabIndex = 6
        Me.PbImgSenha.TabStop = False
        '
        'TxtSenha
        '
        Me.TxtSenha.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSenha.Location = New System.Drawing.Point(89, 200)
        Me.TxtSenha.MaxLength = 10
        Me.TxtSenha.Name = "TxtSenha"
        Me.TxtSenha.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TxtSenha.Size = New System.Drawing.Size(214, 29)
        Me.TxtSenha.TabIndex = 2
        Me.TxtSenha.Text = "Admin"
        Me.TxtSenha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BtnLogin
        '
        Me.BtnLogin.BackColor = System.Drawing.Color.PaleGreen
        Me.BtnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.BtnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnLogin.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLogin.ForeColor = System.Drawing.Color.DarkGreen
        Me.BtnLogin.Image = Global.TecnoStore.My.Resources.Resources._Next
        Me.BtnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnLogin.Location = New System.Drawing.Point(57, 245)
        Me.BtnLogin.Name = "BtnLogin"
        Me.BtnLogin.Size = New System.Drawing.Size(277, 30)
        Me.BtnLogin.TabIndex = 3
        Me.BtnLogin.Text = "LOGIN"
        Me.BtnLogin.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label2.Location = New System.Drawing.Point(175, 180)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Senha"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Location = New System.Drawing.Point(174, 128)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Nome"
        '
        'TxtNomeUsuario
        '
        Me.TxtNomeUsuario.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNomeUsuario.Location = New System.Drawing.Point(89, 148)
        Me.TxtNomeUsuario.MaxLength = 20
        Me.TxtNomeUsuario.Name = "TxtNomeUsuario"
        Me.TxtNomeUsuario.Size = New System.Drawing.Size(214, 29)
        Me.TxtNomeUsuario.TabIndex = 1
        Me.TxtNomeUsuario.Text = "Admin"
        Me.TxtNomeUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PbLogo
        '
        Me.PbLogo.Image = Global.TecnoStore.My.Resources.Resources.LogoTecnoStoreFinal
        Me.PbLogo.Location = New System.Drawing.Point(57, 24)
        Me.PbLogo.Name = "PbLogo"
        Me.PbLogo.Size = New System.Drawing.Size(277, 183)
        Me.PbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PbLogo.TabIndex = 2
        Me.PbLogo.TabStop = False
        '
        'BtnSair
        '
        Me.BtnSair.Font = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSair.Image = Global.TecnoStore.My.Resources.Resources._Exit
        Me.BtnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnSair.Location = New System.Drawing.Point(12, 310)
        Me.BtnSair.Name = "BtnSair"
        Me.BtnSair.Size = New System.Drawing.Size(377, 31)
        Me.BtnSair.TabIndex = 4
        Me.BtnSair.Text = "&Sair"
        Me.BtnSair.UseVisualStyleBackColor = True
        '
        'FrmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(402, 353)
        Me.ControlBox = False
        Me.Controls.Add(Me.GrpLogin)
        Me.Controls.Add(Me.BtnSair)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmLogin"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TecnoStore — Login"
        Me.TopMost = True
        Me.GrpLogin.ResumeLayout(False)
        Me.GrpLogin.PerformLayout()
        CType(Me.PbImgUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbImgSenha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BtnSair As Button
    Friend WithEvents GrpLogin As GroupBox
    Friend WithEvents BtnLogin As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TxtNomeUsuario As TextBox
    Friend WithEvents TxtSenha As TextBox
    Friend WithEvents PbLogo As PictureBox
    Friend WithEvents PbImgUsuario As PictureBox
    Friend WithEvents PbImgSenha As PictureBox
End Class
