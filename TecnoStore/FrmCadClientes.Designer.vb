﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCadClientes
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PnlCadastroDeDados = New System.Windows.Forms.Panel()
        Me.LblUF = New System.Windows.Forms.Label()
        Me.BtnBuscarCEP = New System.Windows.Forms.Button()
        Me.BtnNovo = New System.Windows.Forms.Button()
        Me.BtnSalvar = New System.Windows.Forms.Button()
        Me.TxtCidade = New System.Windows.Forms.TextBox()
        Me.TxtBairro = New System.Windows.Forms.TextBox()
        Me.LblCidade = New System.Windows.Forms.Label()
        Me.LblBairro = New System.Windows.Forms.Label()
        Me.LblComplemento = New System.Windows.Forms.Label()
        Me.TxtComplemento = New System.Windows.Forms.TextBox()
        Me.TxtNumero = New System.Windows.Forms.TextBox()
        Me.TxtRua = New System.Windows.Forms.TextBox()
        Me.LblNumero = New System.Windows.Forms.Label()
        Me.LblRua = New System.Windows.Forms.Label()
        Me.LblCEP = New System.Windows.Forms.Label()
        Me.LblCelular = New System.Windows.Forms.Label()
        Me.LblTelefone = New System.Windows.Forms.Label()
        Me.LblCPF = New System.Windows.Forms.Label()
        Me.MskCEP = New System.Windows.Forms.MaskedTextBox()
        Me.MskCelular = New System.Windows.Forms.MaskedTextBox()
        Me.MskTelefone = New System.Windows.Forms.MaskedTextBox()
        Me.MskCPF = New System.Windows.Forms.MaskedTextBox()
        Me.LblEmail = New System.Windows.Forms.Label()
        Me.LblNomeCliente = New System.Windows.Forms.Label()
        Me.TxtNomeCliente = New System.Windows.Forms.TextBox()
        Me.TxtEmail = New System.Windows.Forms.TextBox()
        Me.TxtIdCliente = New System.Windows.Forms.TextBox()
        Me.LblID = New System.Windows.Forms.Label()
        Me.PnlGerenciaDeDados = New System.Windows.Forms.Panel()
        Me.PbBuscarCliente = New System.Windows.Forms.PictureBox()
        Me.LblBuscarCadastro = New System.Windows.Forms.Label()
        Me.TxtBuscarCadastro = New System.Windows.Forms.TextBox()
        Me.DgvCadClientes = New System.Windows.Forms.DataGridView()
        Me.Selecao = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IdCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NomeCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telefone = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Celular = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Email = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Rua = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Complemento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CEP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bairro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cidade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GrpGerenciamento = New System.Windows.Forms.GroupBox()
        Me.BtnSair = New System.Windows.Forms.Button()
        Me.BtnDeletar = New System.Windows.Forms.Button()
        Me.BtnAlterar = New System.Windows.Forms.Button()
        Me.BtnConsultar = New System.Windows.Forms.Button()
        Me.CboUF = New System.Windows.Forms.ComboBox()
        Me.lblClientesCadastrados = New System.Windows.Forms.Label()
        Me.PnlCadastroDeDados.SuspendLayout()
        Me.PnlGerenciaDeDados.SuspendLayout()
        CType(Me.PbBuscarCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvCadClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpGerenciamento.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlCadastroDeDados
        '
        Me.PnlCadastroDeDados.BackColor = System.Drawing.Color.Gainsboro
        Me.PnlCadastroDeDados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlCadastroDeDados.Controls.Add(Me.LblUF)
        Me.PnlCadastroDeDados.Controls.Add(Me.BtnBuscarCEP)
        Me.PnlCadastroDeDados.Controls.Add(Me.BtnNovo)
        Me.PnlCadastroDeDados.Controls.Add(Me.BtnSalvar)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtCidade)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtBairro)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblCidade)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblBairro)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblComplemento)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtComplemento)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtNumero)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtRua)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblNumero)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblRua)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblCEP)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblCelular)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblTelefone)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblCPF)
        Me.PnlCadastroDeDados.Controls.Add(Me.MskCEP)
        Me.PnlCadastroDeDados.Controls.Add(Me.MskCelular)
        Me.PnlCadastroDeDados.Controls.Add(Me.MskTelefone)
        Me.PnlCadastroDeDados.Controls.Add(Me.MskCPF)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblEmail)
        Me.PnlCadastroDeDados.Controls.Add(Me.LblNomeCliente)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtNomeCliente)
        Me.PnlCadastroDeDados.Controls.Add(Me.TxtEmail)
        Me.PnlCadastroDeDados.Location = New System.Drawing.Point(12, 12)
        Me.PnlCadastroDeDados.Name = "PnlCadastroDeDados"
        Me.PnlCadastroDeDados.Size = New System.Drawing.Size(286, 558)
        Me.PnlCadastroDeDados.TabIndex = 0
        '
        'LblUF
        '
        Me.LblUF.AutoSize = True
        Me.LblUF.Location = New System.Drawing.Point(206, 452)
        Me.LblUF.Name = "LblUF"
        Me.LblUF.Size = New System.Drawing.Size(23, 17)
        Me.LblUF.TabIndex = 31
        Me.LblUF.Text = "UF"
        '
        'BtnBuscarCEP
        '
        Me.BtnBuscarCEP.BackColor = System.Drawing.Color.Gainsboro
        Me.BtnBuscarCEP.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBuscarCEP.Image = Global.TecnoStore.My.Resources.Resources.Find
        Me.BtnBuscarCEP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnBuscarCEP.Location = New System.Drawing.Point(145, 369)
        Me.BtnBuscarCEP.Name = "BtnBuscarCEP"
        Me.BtnBuscarCEP.Size = New System.Drawing.Size(127, 36)
        Me.BtnBuscarCEP.TabIndex = 11
        Me.BtnBuscarCEP.Text = "Buscar CEP"
        Me.BtnBuscarCEP.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnBuscarCEP.UseVisualStyleBackColor = False
        '
        'BtnNovo
        '
        Me.BtnNovo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnNovo.Image = Global.TecnoStore.My.Resources.Resources._1
        Me.BtnNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnNovo.Location = New System.Drawing.Point(10, 15)
        Me.BtnNovo.Name = "BtnNovo"
        Me.BtnNovo.Size = New System.Drawing.Size(262, 37)
        Me.BtnNovo.TabIndex = 0
        Me.BtnNovo.Text = "Novo"
        Me.BtnNovo.UseVisualStyleBackColor = True
        '
        'BtnSalvar
        '
        Me.BtnSalvar.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSalvar.Image = Global.TecnoStore.My.Resources.Resources._22
        Me.BtnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnSalvar.Location = New System.Drawing.Point(10, 516)
        Me.BtnSalvar.Name = "BtnSalvar"
        Me.BtnSalvar.Size = New System.Drawing.Size(262, 32)
        Me.BtnSalvar.TabIndex = 15
        Me.BtnSalvar.Text = "Salvar"
        Me.BtnSalvar.UseVisualStyleBackColor = True
        '
        'TxtCidade
        '
        Me.TxtCidade.Location = New System.Drawing.Point(10, 472)
        Me.TxtCidade.Name = "TxtCidade"
        Me.TxtCidade.Size = New System.Drawing.Size(189, 25)
        Me.TxtCidade.TabIndex = 13
        Me.TxtCidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtBairro
        '
        Me.TxtBairro.Location = New System.Drawing.Point(10, 424)
        Me.TxtBairro.Name = "TxtBairro"
        Me.TxtBairro.Size = New System.Drawing.Size(262, 25)
        Me.TxtBairro.TabIndex = 12
        Me.TxtBairro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblCidade
        '
        Me.LblCidade.AutoSize = True
        Me.LblCidade.Location = New System.Drawing.Point(11, 452)
        Me.LblCidade.Name = "LblCidade"
        Me.LblCidade.Size = New System.Drawing.Size(49, 17)
        Me.LblCidade.TabIndex = 2
        Me.LblCidade.Text = "Cidade"
        '
        'LblBairro
        '
        Me.LblBairro.AutoSize = True
        Me.LblBairro.Location = New System.Drawing.Point(11, 404)
        Me.LblBairro.Name = "LblBairro"
        Me.LblBairro.Size = New System.Drawing.Size(43, 17)
        Me.LblBairro.TabIndex = 15
        Me.LblBairro.Text = "Bairro"
        '
        'LblComplemento
        '
        Me.LblComplemento.AutoSize = True
        Me.LblComplemento.Location = New System.Drawing.Point(11, 308)
        Me.LblComplemento.Name = "LblComplemento"
        Me.LblComplemento.Size = New System.Drawing.Size(90, 17)
        Me.LblComplemento.TabIndex = 14
        Me.LblComplemento.Text = "Complemento"
        '
        'TxtComplemento
        '
        Me.TxtComplemento.Location = New System.Drawing.Point(10, 328)
        Me.TxtComplemento.Name = "TxtComplemento"
        Me.TxtComplemento.Size = New System.Drawing.Size(262, 25)
        Me.TxtComplemento.TabIndex = 9
        Me.TxtComplemento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtNumero
        '
        Me.TxtNumero.Location = New System.Drawing.Point(205, 280)
        Me.TxtNumero.Name = "TxtNumero"
        Me.TxtNumero.Size = New System.Drawing.Size(67, 25)
        Me.TxtNumero.TabIndex = 8
        Me.TxtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtRua
        '
        Me.TxtRua.Location = New System.Drawing.Point(10, 280)
        Me.TxtRua.Name = "TxtRua"
        Me.TxtRua.Size = New System.Drawing.Size(189, 25)
        Me.TxtRua.TabIndex = 7
        Me.TxtRua.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblNumero
        '
        Me.LblNumero.AutoSize = True
        Me.LblNumero.Location = New System.Drawing.Point(206, 260)
        Me.LblNumero.Name = "LblNumero"
        Me.LblNumero.Size = New System.Drawing.Size(56, 17)
        Me.LblNumero.TabIndex = 13
        Me.LblNumero.Text = "Número"
        '
        'LblRua
        '
        Me.LblRua.AutoSize = True
        Me.LblRua.Location = New System.Drawing.Point(11, 260)
        Me.LblRua.Name = "LblRua"
        Me.LblRua.Size = New System.Drawing.Size(30, 17)
        Me.LblRua.TabIndex = 13
        Me.LblRua.Text = "Rua"
        '
        'LblCEP
        '
        Me.LblCEP.AutoSize = True
        Me.LblCEP.Location = New System.Drawing.Point(11, 356)
        Me.LblCEP.Name = "LblCEP"
        Me.LblCEP.Size = New System.Drawing.Size(30, 17)
        Me.LblCEP.TabIndex = 12
        Me.LblCEP.Text = "CEP"
        '
        'LblCelular
        '
        Me.LblCelular.AutoSize = True
        Me.LblCelular.Location = New System.Drawing.Point(142, 164)
        Me.LblCelular.Name = "LblCelular"
        Me.LblCelular.Size = New System.Drawing.Size(48, 17)
        Me.LblCelular.TabIndex = 10
        Me.LblCelular.Text = "Celular"
        '
        'LblTelefone
        '
        Me.LblTelefone.AutoSize = True
        Me.LblTelefone.Location = New System.Drawing.Point(11, 164)
        Me.LblTelefone.Name = "LblTelefone"
        Me.LblTelefone.Size = New System.Drawing.Size(57, 17)
        Me.LblTelefone.TabIndex = 10
        Me.LblTelefone.Text = "Telefone"
        '
        'LblCPF
        '
        Me.LblCPF.AutoSize = True
        Me.LblCPF.Location = New System.Drawing.Point(11, 116)
        Me.LblCPF.Name = "LblCPF"
        Me.LblCPF.Size = New System.Drawing.Size(29, 17)
        Me.LblCPF.TabIndex = 9
        Me.LblCPF.Text = "CPF"
        '
        'MskCEP
        '
        Me.MskCEP.Location = New System.Drawing.Point(10, 376)
        Me.MskCEP.Mask = "00000-000"
        Me.MskCEP.Name = "MskCEP"
        Me.MskCEP.Size = New System.Drawing.Size(129, 25)
        Me.MskCEP.TabIndex = 10
        Me.MskCEP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'MskCelular
        '
        Me.MskCelular.Location = New System.Drawing.Point(143, 184)
        Me.MskCelular.Mask = "(000) 00000-0000"
        Me.MskCelular.Name = "MskCelular"
        Me.MskCelular.Size = New System.Drawing.Size(129, 25)
        Me.MskCelular.TabIndex = 5
        Me.MskCelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'MskTelefone
        '
        Me.MskTelefone.Location = New System.Drawing.Point(10, 184)
        Me.MskTelefone.Mask = "(000) 00000-0000"
        Me.MskTelefone.Name = "MskTelefone"
        Me.MskTelefone.Size = New System.Drawing.Size(117, 25)
        Me.MskTelefone.TabIndex = 4
        Me.MskTelefone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'MskCPF
        '
        Me.MskCPF.Location = New System.Drawing.Point(10, 136)
        Me.MskCPF.Mask = "000,000,000-00"
        Me.MskCPF.Name = "MskCPF"
        Me.MskCPF.Size = New System.Drawing.Size(117, 25)
        Me.MskCPF.TabIndex = 3
        Me.MskCPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Location = New System.Drawing.Point(11, 212)
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(39, 17)
        Me.LblEmail.TabIndex = 2
        Me.LblEmail.Text = "Email"
        '
        'LblNomeCliente
        '
        Me.LblNomeCliente.AutoSize = True
        Me.LblNomeCliente.Location = New System.Drawing.Point(11, 68)
        Me.LblNomeCliente.Name = "LblNomeCliente"
        Me.LblNomeCliente.Size = New System.Drawing.Size(44, 17)
        Me.LblNomeCliente.TabIndex = 2
        Me.LblNomeCliente.Text = "Nome"
        '
        'TxtNomeCliente
        '
        Me.TxtNomeCliente.Location = New System.Drawing.Point(10, 88)
        Me.TxtNomeCliente.MaxLength = 60
        Me.TxtNomeCliente.Multiline = True
        Me.TxtNomeCliente.Name = "TxtNomeCliente"
        Me.TxtNomeCliente.Size = New System.Drawing.Size(262, 25)
        Me.TxtNomeCliente.TabIndex = 2
        Me.TxtNomeCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtEmail
        '
        Me.TxtEmail.Location = New System.Drawing.Point(10, 232)
        Me.TxtEmail.Name = "TxtEmail"
        Me.TxtEmail.Size = New System.Drawing.Size(262, 25)
        Me.TxtEmail.TabIndex = 6
        Me.TxtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtIdCliente
        '
        Me.TxtIdCliente.BackColor = System.Drawing.Color.LightGray
        Me.TxtIdCliente.Location = New System.Drawing.Point(14, 47)
        Me.TxtIdCliente.MaxLength = 9
        Me.TxtIdCliente.Name = "TxtIdCliente"
        Me.TxtIdCliente.Size = New System.Drawing.Size(91, 25)
        Me.TxtIdCliente.TabIndex = 1
        Me.TxtIdCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblID
        '
        Me.LblID.AutoSize = True
        Me.LblID.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblID.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LblID.Location = New System.Drawing.Point(16, 27)
        Me.LblID.Name = "LblID"
        Me.LblID.Size = New System.Drawing.Size(89, 17)
        Me.LblID.TabIndex = 2
        Me.LblID.Text = "ID do Cliente"
        '
        'PnlGerenciaDeDados
        '
        Me.PnlGerenciaDeDados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlGerenciaDeDados.Controls.Add(Me.PbBuscarCliente)
        Me.PnlGerenciaDeDados.Controls.Add(Me.LblBuscarCadastro)
        Me.PnlGerenciaDeDados.Controls.Add(Me.TxtBuscarCadastro)
        Me.PnlGerenciaDeDados.Controls.Add(Me.DgvCadClientes)
        Me.PnlGerenciaDeDados.Controls.Add(Me.GrpGerenciamento)
        Me.PnlGerenciaDeDados.Controls.Add(Me.TxtIdCliente)
        Me.PnlGerenciaDeDados.Controls.Add(Me.LblID)
        Me.PnlGerenciaDeDados.Location = New System.Drawing.Point(295, 25)
        Me.PnlGerenciaDeDados.Name = "PnlGerenciaDeDados"
        Me.PnlGerenciaDeDados.Size = New System.Drawing.Size(678, 545)
        Me.PnlGerenciaDeDados.TabIndex = 1
        '
        'PbBuscarCliente
        '
        Me.PbBuscarCliente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.PbBuscarCliente.Image = Global.TecnoStore.My.Resources.Resources.Find
        Me.PbBuscarCliente.Location = New System.Drawing.Point(636, 17)
        Me.PbBuscarCliente.Name = "PbBuscarCliente"
        Me.PbBuscarCliente.Size = New System.Drawing.Size(19, 27)
        Me.PbBuscarCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbBuscarCliente.TabIndex = 30
        Me.PbBuscarCliente.TabStop = False
        '
        'LblBuscarCadastro
        '
        Me.LblBuscarCadastro.AutoSize = True
        Me.LblBuscarCadastro.Location = New System.Drawing.Point(258, 27)
        Me.LblBuscarCadastro.Name = "LblBuscarCadastro"
        Me.LblBuscarCadastro.Size = New System.Drawing.Size(339, 17)
        Me.LblBuscarCadastro.TabIndex = 25
        Me.LblBuscarCadastro.Text = "Buscar por um cadastro específico | *busque pelo Nome"
        '
        'TxtBuscarCadastro
        '
        Me.TxtBuscarCadastro.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.TxtBuscarCadastro.Location = New System.Drawing.Point(261, 47)
        Me.TxtBuscarCadastro.Name = "TxtBuscarCadastro"
        Me.TxtBuscarCadastro.Size = New System.Drawing.Size(394, 25)
        Me.TxtBuscarCadastro.TabIndex = 1
        Me.TxtBuscarCadastro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DgvCadClientes
        '
        Me.DgvCadClientes.AllowUserToAddRows = False
        Me.DgvCadClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvCadClientes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Selecao, Me.IdCliente, Me.NomeCliente, Me.CPF, Me.Telefone, Me.Celular, Me.Email, Me.Rua, Me.Numero, Me.Complemento, Me.CEP, Me.Bairro, Me.Cidade, Me.UF})
        Me.DgvCadClientes.Location = New System.Drawing.Point(8, 151)
        Me.DgvCadClientes.Name = "DgvCadClientes"
        Me.DgvCadClientes.Size = New System.Drawing.Size(647, 384)
        Me.DgvCadClientes.TabIndex = 0
        '
        'Selecao
        '
        Me.Selecao.HeaderText = "Seleção"
        Me.Selecao.Name = "Selecao"
        '
        'IdCliente
        '
        Me.IdCliente.HeaderText = "Id"
        Me.IdCliente.Name = "IdCliente"
        Me.IdCliente.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.IdCliente.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'NomeCliente
        '
        Me.NomeCliente.HeaderText = "Nome"
        Me.NomeCliente.Name = "NomeCliente"
        Me.NomeCliente.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.NomeCliente.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'CPF
        '
        Me.CPF.HeaderText = "CPF"
        Me.CPF.Name = "CPF"
        Me.CPF.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CPF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Telefone
        '
        Me.Telefone.HeaderText = "Telefone"
        Me.Telefone.Name = "Telefone"
        Me.Telefone.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Telefone.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Celular
        '
        Me.Celular.HeaderText = "Celular"
        Me.Celular.Name = "Celular"
        Me.Celular.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Celular.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Email
        '
        Me.Email.HeaderText = "Email"
        Me.Email.Name = "Email"
        '
        'Rua
        '
        Me.Rua.HeaderText = "Rua"
        Me.Rua.Name = "Rua"
        '
        'Numero
        '
        Me.Numero.HeaderText = "Número"
        Me.Numero.Name = "Numero"
        '
        'Complemento
        '
        Me.Complemento.HeaderText = "Complemento"
        Me.Complemento.Name = "Complemento"
        '
        'CEP
        '
        Me.CEP.HeaderText = "CEP"
        Me.CEP.Name = "CEP"
        '
        'Bairro
        '
        Me.Bairro.HeaderText = "Bairro"
        Me.Bairro.Name = "Bairro"
        '
        'Cidade
        '
        Me.Cidade.HeaderText = "Cidade"
        Me.Cidade.Name = "Cidade"
        '
        'UF
        '
        Me.UF.HeaderText = "UF"
        Me.UF.Name = "UF"
        '
        'GrpGerenciamento
        '
        Me.GrpGerenciamento.BackColor = System.Drawing.Color.Linen
        Me.GrpGerenciamento.Controls.Add(Me.BtnSair)
        Me.GrpGerenciamento.Controls.Add(Me.BtnDeletar)
        Me.GrpGerenciamento.Controls.Add(Me.BtnAlterar)
        Me.GrpGerenciamento.Controls.Add(Me.BtnConsultar)
        Me.GrpGerenciamento.Location = New System.Drawing.Point(8, 78)
        Me.GrpGerenciamento.Name = "GrpGerenciamento"
        Me.GrpGerenciamento.Size = New System.Drawing.Size(647, 67)
        Me.GrpGerenciamento.TabIndex = 29
        Me.GrpGerenciamento.TabStop = False
        Me.GrpGerenciamento.Text = "Gerenciamento"
        '
        'BtnSair
        '
        Me.BtnSair.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSair.Image = Global.TecnoStore.My.Resources.Resources._Exit
        Me.BtnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnSair.Location = New System.Drawing.Point(501, 25)
        Me.BtnSair.Name = "BtnSair"
        Me.BtnSair.Size = New System.Drawing.Size(139, 36)
        Me.BtnSair.TabIndex = 28
        Me.BtnSair.Text = "Sair"
        Me.BtnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnSair.UseVisualStyleBackColor = True
        '
        'BtnDeletar
        '
        Me.BtnDeletar.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDeletar.Image = Global.TecnoStore.My.Resources.Resources.Delete
        Me.BtnDeletar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnDeletar.Location = New System.Drawing.Point(328, 24)
        Me.BtnDeletar.Name = "BtnDeletar"
        Me.BtnDeletar.Size = New System.Drawing.Size(167, 36)
        Me.BtnDeletar.TabIndex = 27
        Me.BtnDeletar.Text = "Deletar"
        Me.BtnDeletar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDeletar.UseVisualStyleBackColor = True
        '
        'BtnAlterar
        '
        Me.BtnAlterar.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAlterar.Image = Global.TecnoStore.My.Resources.Resources.Sync
        Me.BtnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnAlterar.Location = New System.Drawing.Point(150, 24)
        Me.BtnAlterar.Name = "BtnAlterar"
        Me.BtnAlterar.Size = New System.Drawing.Size(172, 36)
        Me.BtnAlterar.TabIndex = 26
        Me.BtnAlterar.Text = "Alterar"
        Me.BtnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnAlterar.UseVisualStyleBackColor = True
        '
        'BtnConsultar
        '
        Me.BtnConsultar.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnConsultar.Image = Global.TecnoStore.My.Resources.Resources.Database
        Me.BtnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnConsultar.Location = New System.Drawing.Point(6, 24)
        Me.BtnConsultar.Name = "BtnConsultar"
        Me.BtnConsultar.Size = New System.Drawing.Size(139, 36)
        Me.BtnConsultar.TabIndex = 25
        Me.BtnConsultar.Text = "Consultar"
        Me.BtnConsultar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnConsultar.UseVisualStyleBackColor = True
        '
        'CboUF
        '
        Me.CboUF.FormattingEnabled = True
        Me.CboUF.Items.AddRange(New Object() {"AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO"})
        Me.CboUF.Location = New System.Drawing.Point(218, 485)
        Me.CboUF.Name = "CboUF"
        Me.CboUF.Size = New System.Drawing.Size(67, 25)
        Me.CboUF.TabIndex = 14
        '
        'lblClientesCadastrados
        '
        Me.lblClientesCadastrados.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lblClientesCadastrados.AutoSize = True
        Me.lblClientesCadastrados.BackColor = System.Drawing.SystemColors.Control
        Me.lblClientesCadastrados.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientesCadastrados.Location = New System.Drawing.Point(527, 9)
        Me.lblClientesCadastrados.Name = "lblClientesCadastrados"
        Me.lblClientesCadastrados.Size = New System.Drawing.Size(256, 21)
        Me.lblClientesCadastrados.TabIndex = 24
        Me.lblClientesCadastrados.Text = "Clientes Cadastrados no Sistema"
        '
        'FrmCadClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(985, 580)
        Me.Controls.Add(Me.lblClientesCadastrados)
        Me.Controls.Add(Me.CboUF)
        Me.Controls.Add(Me.PnlCadastroDeDados)
        Me.Controls.Add(Me.PnlGerenciaDeDados)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "FrmCadClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TecnoStore — Cadastro de Clientes"
        Me.PnlCadastroDeDados.ResumeLayout(False)
        Me.PnlCadastroDeDados.PerformLayout()
        Me.PnlGerenciaDeDados.ResumeLayout(False)
        Me.PnlGerenciaDeDados.PerformLayout()
        CType(Me.PbBuscarCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvCadClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpGerenciamento.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PnlCadastroDeDados As Panel
    Friend WithEvents PnlGerenciaDeDados As Panel
    Friend WithEvents DgvCadClientes As DataGridView
    Friend WithEvents LblEmail As Label
    Friend WithEvents LblNomeCliente As Label
    Friend WithEvents TxtIdCliente As TextBox
    Friend WithEvents TxtNomeCliente As TextBox
    Friend WithEvents TxtEmail As TextBox
    Friend WithEvents LblID As Label
    Friend WithEvents LblNumero As Label
    Friend WithEvents LblRua As Label
    Friend WithEvents LblCEP As Label
    Friend WithEvents LblCelular As Label
    Friend WithEvents LblTelefone As Label
    Friend WithEvents MskCEP As MaskedTextBox
    Friend WithEvents MskCelular As MaskedTextBox
    Friend WithEvents MskTelefone As MaskedTextBox
    Friend WithEvents MskCPF As MaskedTextBox
    Friend WithEvents TxtRua As TextBox
    Friend WithEvents TxtNumero As TextBox
    Friend WithEvents TxtCidade As TextBox
    Friend WithEvents TxtBairro As TextBox
    Friend WithEvents LblCidade As Label
    Friend WithEvents LblBairro As Label
    Friend WithEvents LblComplemento As Label
    Friend WithEvents TxtComplemento As TextBox
    Friend WithEvents CboUF As ComboBox
    Friend WithEvents BtnSalvar As Button
    Friend WithEvents BtnNovo As Button
    Friend WithEvents lblClientesCadastrados As Label
    Friend WithEvents BtnBuscarCEP As Button
    Friend WithEvents TxtBuscarCadastro As TextBox
    Friend WithEvents LblBuscarCadastro As Label
    Friend WithEvents BtnConsultar As Button
    Friend WithEvents BtnAlterar As Button
    Friend WithEvents BtnDeletar As Button
    Friend WithEvents BtnSair As Button
    Friend WithEvents GrpGerenciamento As GroupBox
    Friend WithEvents PbBuscarCliente As PictureBox
    Friend WithEvents LblUF As Label
    Friend WithEvents LblCPF As Label
    Friend WithEvents Selecao As DataGridViewCheckBoxColumn
    Friend WithEvents IdCliente As DataGridViewTextBoxColumn
    Friend WithEvents NomeCliente As DataGridViewTextBoxColumn
    Friend WithEvents CPF As DataGridViewTextBoxColumn
    Friend WithEvents Telefone As DataGridViewTextBoxColumn
    Friend WithEvents Celular As DataGridViewTextBoxColumn
    Friend WithEvents Rua As DataGridViewTextBoxColumn
    Friend WithEvents Numero As DataGridViewTextBoxColumn
    Friend WithEvents Complemento As DataGridViewTextBoxColumn
    Friend WithEvents CEP As DataGridViewTextBoxColumn
    Friend WithEvents Bairro As DataGridViewTextBoxColumn
    Friend WithEvents Cidade As DataGridViewTextBoxColumn
    Friend WithEvents UF As DataGridViewTextBoxColumn
    Friend WithEvents Email As DataGridViewTextBoxColumn
End Class
