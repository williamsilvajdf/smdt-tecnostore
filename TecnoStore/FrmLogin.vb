﻿Option Strict Off

Public Class FrmLogin
    Public TbUsuarios As DAO.Recordset()
    Dim SQL As String
    Dim Usuario, Senha As String

    Private Sub FrmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AbrirBase() 'Instancia a base de dados (Access)
    End Sub

    Private Sub ConsultarCadastro()
        Try
            SQL = "SELECT * FROM TbUsuarios WHERE NomeUsuario LIKE '*" & TxtNomeUsuario.Text & "*' AND SenhaUsuario LIKE '*" & TxtSenha.Text & "*' AND Ativo = True"
            'Nessa consulta SQL a Pesquisa irá buscar na Tabela Usuários todos os campos onde o NomeUsuario e a SenhaUsuario sejam exatamente os dados inseridos nos TextBoxes.
            ''continuação: e a propriedade Ativo deve ser True para que o Usuário seja visível para o processo de Login.
            Pesquisa = Base.OpenRecordset(SQL)
            Usuario = Pesquisa(1).Value 'passa o valor do campo [NomeUsuario contido no Access] para a variável [Usuario no VB.Net]
            Senha = Pesquisa(2).Value 'passa o valor do campo [SenhaUsuario contido no Access] para a variável [Senha no VB.Net]
            Pesquisa.Close() 'Fecha a consulta sql

            If TxtNomeUsuario.Text = Usuario And TxtSenha.Text = Senha Then
                Close() 'Pecha o formulário de login
                MdiPrincipal.Show()
                NomeUsuario = Usuario 'Passa para a variável o nome do usuário logado c/ o propósito de exibí-lo no rodapé do FrmPrincipal
                HorarioLogin = DateTime.Now.ToString("HH:mm:ss") 'Passa ao componente TsrLoginGerenc o horário de entrada no sistema
            End If

        Catch ex As Exception 'caso os os dados de Login não estejam na base [Access <---> TbUsuarios]
            MsgBox("Os dados informados estão incorretos!" + vbCrLf + "Se o erro persistir contate o administrador do sistema.", vbExclamation, "Usuário ou Senha incorretos")
            Exit Sub
        End Try
    End Sub

    Private Sub BtnSair_Click(sender As Object, e As EventArgs) Handles BtnSair.Click
        MdiPrincipal.Close() 'Fecha o formulário Principal que estava aguardando o Login
        Close() 'fecha o formulário de Login
    End Sub

    Private Sub BtnLogin_Click(sender As Object, e As EventArgs) Handles BtnLogin.Click
        ConsultarCadastro() 'Verifica se os dados inseridos constam na base para efetuar o login
    End Sub
End Class