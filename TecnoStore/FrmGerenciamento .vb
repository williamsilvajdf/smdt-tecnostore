﻿Option Strict Off

Public Class FrmGerenciamento
    Dim TbClientes As DAO.Recordset()
    Dim TbProdutos As DAO.Recordset()
    Dim TbServicos As DAO.Recordset()
    Dim TbVendas As DAO.Recordset()
    Dim TbDetalheProdutos As DAO.Recordset()
    Dim TbDetalheServicos As DAO.Recordset()
    Dim SQL As String
    Dim X As Integer
    Dim Existe As Boolean

    Private Sub FrmGerenciamento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LblUsuarioLogado.Text = NomeUsuario
        LblHorarioLogin.Text = HorarioLogin 'Exibe a hora atual -- estaticamente, 
        'para exibir dinamicamente é necessário adicionar um Timer.
    End Sub

    Private Sub BtnSair_Click(sender As Object, e As EventArgs) Handles BtnSair.Click
        Close() 'Fecha o FrmGerenciamento 
    End Sub
End Class