﻿Public Class MdiPrincipal
    Private Sub MdiPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AbrirBase() 'Torna acessível o banco de dados contido no Access
        FrmLogin.ShowDialog() 'Exibe o formulário de Login antes do Principal
        LblNomeUsuario.Text = NomeUsuario 'Retorna o Nome do Usuário logado
    End Sub

    Private Sub BtnProdutos_Click(sender As Object, e As EventArgs) Handles BtnProdutos.Click
        FrmCadProdutos.Show() 'Chama o formulário de Cadastro de Produtos
    End Sub

    Private Sub BtnServicos_Click(sender As Object, e As EventArgs) Handles BtnServicos.Click
        FrmCadServicos.Show() 'Chama o formulário de Cadastro de Serviços
    End Sub

    Private Sub BtnClientes_Click(sender As Object, e As EventArgs) Handles BtnClientes.Click
        FrmCadClientes.Show() 'Chama o formulário de Cadastro de Clientes
    End Sub

    Private Sub BtnGerenciamento_Click(sender As Object, e As EventArgs) Handles BtnGerenciamento.Click
        FrmGerenciamento.Show() 'Chama o formulário de Gerenciamento
    End Sub


    Private Sub BtnDeslogar_Click(sender As Object, e As EventArgs) Handles BtnDeslogar.Click
        LblNomeUsuario.Text = "" 'Limpa o Label NomeUsuario
        FrmLogin.ShowDialog() 'Exibe o formulário de Login de forma que o FrmPrincipal não possa ser focado até o login retornar True
        LblNomeUsuario.Text = NomeUsuario 'Atualiza o LblNomeUsuario com o Nome do novo logon
    End Sub

    Private Sub Sair()
        If MsgBox("Deseja parar a execução agora?", vbYesNo, "Confirmar ação") = vbYes Then
            Application.ExitThread() 'Finaliza a execução do programa
        End If
    End Sub

    Private Sub BtnSair_Click(sender As Object, e As EventArgs) Handles BtnSair.Click
        Sair()
    End Sub

    Private Sub TsrSair_Click(sender As Object, e As EventArgs) Handles TsrSair.Click
        Sair()
    End Sub
End Class
