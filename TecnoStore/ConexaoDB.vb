﻿Option Strict On

Module ConexaoDB
    Public A As New DAO.DBEngine
    Public Base As DAO.Database
    Public Pesquisa, Consulta As DAO.Recordset
    Public B As Integer

    Public Sub AbrirBase()
        Base = A.Workspaces(0).OpenDatabase(My.Application.Info.DirectoryPath & "\TecnoStoreDB.mdb", False, False)
        Consulta = Base.OpenRecordset("TbUsuarios")
        Consulta = Base.OpenRecordset("TbProdutos")
        Consulta = Base.OpenRecordset("TbServicos")
        Consulta = Base.OpenRecordset("TbClientes")
    End Sub

    Public NomeUsuario As String 'Retorna ao TsrLoginPrincipal o Nome do Usuário Logado
    Public HorarioLogin As String 'Retorna ao TsrLoginGerenc o horário em que o usuário entrou
End Module
