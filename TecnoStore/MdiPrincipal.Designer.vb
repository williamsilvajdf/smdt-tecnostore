﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MdiPrincipal
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: O procedimento a seguir é exigido pelo Windows Form Designer
    'Ele pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MdiPrincipal))
        Me.MnuPrincipal = New System.Windows.Forms.MenuStrip()
        Me.mnuArquivo = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsrSeparador = New System.Windows.Forms.ToolStripSeparator()
        Me.PnlPrincipal = New System.Windows.Forms.Panel()
        Me.LblSistemaDeGerenciamento = New System.Windows.Forms.Label()
        Me.TsrLoginPrincipal = New System.Windows.Forms.ToolStrip()
        Me.LblNomeUsuario = New System.Windows.Forms.ToolStripLabel()
        Me.LblTrocarUsuario = New System.Windows.Forms.ToolStripLabel()
        Me.LblLegendaUsuarioLogado = New System.Windows.Forms.ToolStripLabel()
        Me.BtnDeslogar = New System.Windows.Forms.ToolStripButton()
        Me.BtnClientes = New System.Windows.Forms.Button()
        Me.BtnGerenciamento = New System.Windows.Forms.Button()
        Me.BtnProdutos = New System.Windows.Forms.Button()
        Me.BtnServicos = New System.Windows.Forms.Button()
        Me.TsrSobre = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsrSair = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnSair = New System.Windows.Forms.Button()
        Me.PbLogo = New System.Windows.Forms.PictureBox()
        Me.MnuPrincipal.SuspendLayout()
        Me.PnlPrincipal.SuspendLayout()
        Me.TsrLoginPrincipal.SuspendLayout()
        CType(Me.PbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MnuPrincipal
        '
        Me.MnuPrincipal.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuArquivo})
        Me.MnuPrincipal.Location = New System.Drawing.Point(0, 0)
        Me.MnuPrincipal.Name = "MnuPrincipal"
        Me.MnuPrincipal.Size = New System.Drawing.Size(525, 24)
        Me.MnuPrincipal.TabIndex = 5
        '
        'mnuArquivo
        '
        Me.mnuArquivo.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsrSobre, Me.TsrSeparador, Me.TsrSair})
        Me.mnuArquivo.Name = "mnuArquivo"
        Me.mnuArquivo.Size = New System.Drawing.Size(61, 20)
        Me.mnuArquivo.Text = "Arquivo"
        '
        'TsrSeparador
        '
        Me.TsrSeparador.Name = "TsrSeparador"
        Me.TsrSeparador.Size = New System.Drawing.Size(177, 6)
        '
        'PnlPrincipal
        '
        Me.PnlPrincipal.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.PnlPrincipal.Controls.Add(Me.BtnClientes)
        Me.PnlPrincipal.Controls.Add(Me.BtnGerenciamento)
        Me.PnlPrincipal.Controls.Add(Me.BtnProdutos)
        Me.PnlPrincipal.Controls.Add(Me.BtnServicos)
        Me.PnlPrincipal.Location = New System.Drawing.Point(0, 216)
        Me.PnlPrincipal.Name = "PnlPrincipal"
        Me.PnlPrincipal.Size = New System.Drawing.Size(525, 166)
        Me.PnlPrincipal.TabIndex = 4
        '
        'LblSistemaDeGerenciamento
        '
        Me.LblSistemaDeGerenciamento.AutoSize = True
        Me.LblSistemaDeGerenciamento.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.LblSistemaDeGerenciamento.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSistemaDeGerenciamento.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LblSistemaDeGerenciamento.Location = New System.Drawing.Point(46, 38)
        Me.LblSistemaDeGerenciamento.Name = "LblSistemaDeGerenciamento"
        Me.LblSistemaDeGerenciamento.Size = New System.Drawing.Size(442, 25)
        Me.LblSistemaDeGerenciamento.TabIndex = 6
        Me.LblSistemaDeGerenciamento.Text = "Sistema de Gerenciamento de Loja de Informática"
        '
        'TsrLoginPrincipal
        '
        Me.TsrLoginPrincipal.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.TsrLoginPrincipal.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TsrLoginPrincipal.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsrLoginPrincipal.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LblLegendaUsuarioLogado, Me.LblNomeUsuario, Me.BtnDeslogar, Me.LblTrocarUsuario})
        Me.TsrLoginPrincipal.Location = New System.Drawing.Point(0, 433)
        Me.TsrLoginPrincipal.Name = "TsrLoginPrincipal"
        Me.TsrLoginPrincipal.Size = New System.Drawing.Size(525, 25)
        Me.TsrLoginPrincipal.TabIndex = 8
        '
        'LblNomeUsuario
        '
        Me.LblNomeUsuario.Name = "LblNomeUsuario"
        Me.LblNomeUsuario.Size = New System.Drawing.Size(47, 22)
        Me.LblNomeUsuario.Text = "Usuario"
        '
        'LblTrocarUsuario
        '
        Me.LblTrocarUsuario.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.LblTrocarUsuario.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTrocarUsuario.Name = "LblTrocarUsuario"
        Me.LblTrocarUsuario.Size = New System.Drawing.Size(90, 22)
        Me.LblTrocarUsuario.Text = "Trocar Usuário:"
        '
        'LblLegendaUsuarioLogado
        '
        Me.LblLegendaUsuarioLogado.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLegendaUsuarioLogado.Image = Global.TecnoStore.My.Resources.Resources.User
        Me.LblLegendaUsuarioLogado.Name = "LblLegendaUsuarioLogado"
        Me.LblLegendaUsuarioLogado.Size = New System.Drawing.Size(111, 22)
        Me.LblLegendaUsuarioLogado.Text = "Usuário Logado:"
        Me.LblLegendaUsuarioLogado.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal
        '
        'BtnDeslogar
        '
        Me.BtnDeslogar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BtnDeslogar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtnDeslogar.Image = Global.TecnoStore.My.Resources.Resources.Turn_off
        Me.BtnDeslogar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtnDeslogar.Name = "BtnDeslogar"
        Me.BtnDeslogar.Size = New System.Drawing.Size(23, 22)
        Me.BtnDeslogar.Text = "Deslogar"
        '
        'BtnClientes
        '
        Me.BtnClientes.BackColor = System.Drawing.Color.Gainsboro
        Me.BtnClientes.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnClientes.Image = Global.TecnoStore.My.Resources.Resources.User_group
        Me.BtnClientes.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnClientes.Location = New System.Drawing.Point(12, 82)
        Me.BtnClientes.Name = "BtnClientes"
        Me.BtnClientes.Size = New System.Drawing.Size(250, 72)
        Me.BtnClientes.TabIndex = 3
        Me.BtnClientes.Text = "Clientes"
        Me.BtnClientes.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnClientes.UseVisualStyleBackColor = False
        '
        'BtnGerenciamento
        '
        Me.BtnGerenciamento.BackColor = System.Drawing.Color.Gainsboro
        Me.BtnGerenciamento.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnGerenciamento.Image = Global.TecnoStore.My.Resources.Resources.Computer
        Me.BtnGerenciamento.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnGerenciamento.Location = New System.Drawing.Point(262, 82)
        Me.BtnGerenciamento.Name = "BtnGerenciamento"
        Me.BtnGerenciamento.Size = New System.Drawing.Size(251, 72)
        Me.BtnGerenciamento.TabIndex = 0
        Me.BtnGerenciamento.Text = "Gerenciamento"
        Me.BtnGerenciamento.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnGerenciamento.UseVisualStyleBackColor = False
        '
        'BtnProdutos
        '
        Me.BtnProdutos.BackColor = System.Drawing.Color.Gainsboro
        Me.BtnProdutos.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnProdutos.Image = Global.TecnoStore.My.Resources.Resources.Goods
        Me.BtnProdutos.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnProdutos.Location = New System.Drawing.Point(12, 12)
        Me.BtnProdutos.Name = "BtnProdutos"
        Me.BtnProdutos.Size = New System.Drawing.Size(250, 68)
        Me.BtnProdutos.TabIndex = 1
        Me.BtnProdutos.Text = "Produtos"
        Me.BtnProdutos.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnProdutos.UseVisualStyleBackColor = False
        '
        'BtnServicos
        '
        Me.BtnServicos.BackColor = System.Drawing.Color.Gainsboro
        Me.BtnServicos.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnServicos.Image = Global.TecnoStore.My.Resources.Resources.Careers
        Me.BtnServicos.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnServicos.Location = New System.Drawing.Point(262, 12)
        Me.BtnServicos.Name = "BtnServicos"
        Me.BtnServicos.Size = New System.Drawing.Size(251, 68)
        Me.BtnServicos.TabIndex = 2
        Me.BtnServicos.Text = "Serviços"
        Me.BtnServicos.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.BtnServicos.UseVisualStyleBackColor = False
        '
        'TsrSobre
        '
        Me.TsrSobre.Image = Global.TecnoStore.My.Resources.Resources.About
        Me.TsrSobre.Name = "TsrSobre"
        Me.TsrSobre.Size = New System.Drawing.Size(180, 22)
        Me.TsrSobre.Text = "Sobre"
        '
        'TsrSair
        '
        Me.TsrSair.Image = Global.TecnoStore.My.Resources.Resources._Exit
        Me.TsrSair.Name = "TsrSair"
        Me.TsrSair.Size = New System.Drawing.Size(180, 22)
        Me.TsrSair.Text = "Sair"
        '
        'BtnSair
        '
        Me.BtnSair.BackColor = System.Drawing.Color.LightGray
        Me.BtnSair.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSair.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.BtnSair.Image = Global.TecnoStore.My.Resources.Resources._Exit
        Me.BtnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnSair.Location = New System.Drawing.Point(12, 388)
        Me.BtnSair.Name = "BtnSair"
        Me.BtnSair.Size = New System.Drawing.Size(501, 32)
        Me.BtnSair.TabIndex = 4
        Me.BtnSair.Text = "SAIR"
        Me.BtnSair.UseVisualStyleBackColor = False
        '
        'PbLogo
        '
        Me.PbLogo.BackColor = System.Drawing.Color.Silver
        Me.PbLogo.Image = Global.TecnoStore.My.Resources.Resources.LogoTecnoStoreFinal
        Me.PbLogo.Location = New System.Drawing.Point(0, 75)
        Me.PbLogo.Name = "PbLogo"
        Me.PbLogo.Size = New System.Drawing.Size(525, 239)
        Me.PbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbLogo.TabIndex = 3
        Me.PbLogo.TabStop = False
        '
        'MdiPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ClientSize = New System.Drawing.Size(525, 458)
        Me.Controls.Add(Me.TsrLoginPrincipal)
        Me.Controls.Add(Me.PnlPrincipal)
        Me.Controls.Add(Me.LblSistemaDeGerenciamento)
        Me.Controls.Add(Me.MnuPrincipal)
        Me.Controls.Add(Me.BtnSair)
        Me.Controls.Add(Me.PbLogo)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MnuPrincipal
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "MdiPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TecnoStore"
        Me.MnuPrincipal.ResumeLayout(False)
        Me.MnuPrincipal.PerformLayout()
        Me.PnlPrincipal.ResumeLayout(False)
        Me.TsrLoginPrincipal.ResumeLayout(False)
        Me.TsrLoginPrincipal.PerformLayout()
        CType(Me.PbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MnuPrincipal As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuArquivo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsrSobre As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsrSeparador As ToolStripSeparator
    Friend WithEvents TsrSair As ToolStripMenuItem
    Friend WithEvents PbLogo As PictureBox
    Friend WithEvents PnlPrincipal As Panel
    Friend WithEvents BtnGerenciamento As Button
    Friend WithEvents BtnProdutos As Button
    Friend WithEvents BtnServicos As Button
    Friend WithEvents BtnClientes As Button
    Friend WithEvents BtnSair As Button
    Friend WithEvents LblSistemaDeGerenciamento As Label
    Friend WithEvents TsrLoginPrincipal As ToolStrip
    Friend WithEvents LblNomeUsuario As ToolStripLabel
    Friend WithEvents LblLegendaUsuarioLogado As ToolStripLabel
    Friend WithEvents BtnDeslogar As ToolStripButton
    Friend WithEvents LblTrocarUsuario As ToolStripLabel
End Class
